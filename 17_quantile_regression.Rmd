---
title: Quantile regression
author: Tom Parker
date: \today
output:
  binb::metropolis:
    includes:
      in_header: custom.tex
fontsize: 11pt
---

```{r,setup, include=FALSE}
knitr::opts_chunk$set(cache=TRUE)
```

# Population model

## Quantiles
- The term *quantile* is meant to generalize several "x-ile" words that are in
  common use: quartile, quintile, decile, percentile...
- e.g. in a sample, the first quartile is a point that sits above 25\% of
  observations.  The median has \(1/2\) of the observations below and above it.
  The 37th percentile has 37% of observations below it.
- For RV \(X\) and $\tau \in (0, 1)$, the **\(\tau\)th quantile** is the \(x^*\)
  that is greater than proportion \(\tau\) and less than proportion \((1-\tau)\)
  of the distribution.
- The quantile function is almost the inverse of the distribution function.
  Recall the DF \(F: \mathbb{R} \rightarrow [0, 1]\) is 
  \begin{equation*}
    F(x) = \prob{X \leq x}.
  \end{equation*}
  \(Q: [0, 1] \rightarrow \mathbb{R}\) and 
  \begin{equation*}
    Q(\tau) = \inf \{x : F(x) \geq \tau \}.
  \end{equation*}

## Quantile function illustration

```{r qpics, echo=FALSE, fig.height=5}
tau <- 0.6
d <- 4
xtop <- qchisq(.99, df=d)
xst <- qchisq(tau, df=d)

par(mfrow=c(1, 3), mar=c(3.1, 3.1, 1.1, 1.1), mgp=2:0)
curve(dchisq(x, df=d), from=0, to=xtop, main="density", ylab=expression(f(x)))
segments(xst, 0, xst, dchisq(xst, df=d), col="blue")
arrows(6, .15, 2, .11, length=.05)
text(6, .16, expression(tau))
arrows(10, .09, 6, .04, length=.05)
text(10, .10, expression(1 - tau))

curve(pchisq(x, df=d), from=0, to=xtop, main="cdf", ylab=expression(F(x)))
segments(xst, 0, xst, tau, col="blue")
segments(xst, tau, 0, tau, col="blue", lty=2)
axis(2, at=tau, labels=expression(tau), las=2)

curve(qchisq(x, df=d), from=0, to=1, main="quantile fun.",
ylab=expression(F^(-1)(tau)))
segments(tau, 0, tau, xst, col="blue", lty=2)
segments(tau, xst, 0, xst, col="blue")
axis(1, at=tau, labels=expression(tau))
```

## What makes quantiles attractive?
1. Because it is the inverse of the CDF of a random variable, the quantile
  function tells you everything that the CDF does.
  - The answers are points in the distribution instead of probabilities, which
    is nice to interpret.
2. Quantiles are **robust**, in a different sense: the median of 
   $$ 1, 2, 3, 4, 5 $$
   is the same as the median of
   $$ 1, 2, 3, 4, 5000. $$
   - In the statistics literature, this is known as *robustness* (to outlying
     observations).

## How should we connect quantiles to regression quantiles?
- Look at the usual regression estimator.
- Remember that regression generalizes *averaging*: 
  1. We can show that
  \begin{equation*}
    \argmin_{m} \ex{(X - m)^2} = \ex{X}.
  \end{equation*}
  2. We can replace the $m$ in the above optimisation problem with a function of
     covariates: if
  \begin{equation*}
    \ex{Y_i | X_i = x} = x\tr \beta,
  \end{equation*}
  then
  \begin{equation*}
    \argmin_{b} \ex{(Y_i - X_i\tr b)^2} = \beta.
  \end{equation*}
- *Optimisation* replaces some messy (hypothetical) process of combining many
  conditional averages to get a regression line.

## Population quantiles via optimisation
- The big trick: define the *check function*
  \begin{equation*}
    \rho_{\tau}(x) = x(\tau - I(x < 0)).
  \end{equation*}
- If \(X\) has quantile function \(Q\), then 
  $$\argmin_{m} \ex{\rho_{\tau}(X - m)} = Q(\tau).$$
- Just like with mean regression, we can say that if
  \begin{equation*}
    Q_{Y_i | X_i}(\tau | X_i = x) = x\tr \beta,
  \end{equation*}
  then
  \begin{equation*}
    \argmin_{b} \ex{\rho_\tau(Y_i - X_i\tr b)} = \beta.
  \end{equation*}

## Optimisation (details)
- The second equality uses [\alert{Leibniz'
  rule}](https://en.wikipedia.org/wiki/Leibniz_integral_rule).
\begin{align*}
  \frac{\partial}{\partial m} \ex{\rho_\tau(X - m)} &=
  \frac{\partial}{\partial m} (\tau - 1) \int_{-\infty}^m (x - m) \ud F(x)
  + \tau \int_{m}^{\infty} (x - m) \ud F(x) \\ 
  {} &= - (\tau - 1) \int_{-\infty}^m \ud F(x) - \tau \int_{m}^{\infty} \ud
  F(x) \\
  {} &= -(\tau - 1) F(m) - \tau (1 - F(m)) \\
  {} &\stackrel{set}{=} 0
\end{align*}
- Then solving for $m$ implies \(F(m) = \tau\) or \(F^{-1}(\tau) = m\).

## Objective functions 
- Here are two pictures of the check function (the right one varies $\tau$)

```{r objfuns, echo=FALSE, fig.align='center', fig.height = 5}
tau <- 0.6
eps <- 0.15
cfun <- function(u, tau) {u * (tau - ifelse(u < 0, 1, 0))}
par(mfrow=c(1, 2), mar=c(3.1, 3.1, 1.1, 1.1), mgp=2:0)
curve(cfun(x, 0.6), xlim = c(-1, 1), ylim = 0:1, col="chocolate2",
      main = expression(paste("objective function: ", tau == 0.6)), ylab =
      expression(rho[0.6]))
abline(h=0, lty=3, lwd=0.75)
segments(0, 0, 0, 1, lty=3, lwd=0.75)
segments(-0.5, cfun(-0.5, tau), -0.5, cfun(-0.5 + eps, tau))
segments(-0.5, cfun(-0.5 + eps, tau), -0.5 + eps, cfun(-0.5 + eps, tau))
text(-0.5 - .2, cfun(-0.5 + eps, tau), expression(tau-1))
segments(0.5, cfun(0.5, tau), 0.5 + eps, cfun(0.5, tau))
segments(0.5 + eps, cfun(0.5, tau), 0.5 + eps, cfun(0.5 + eps, tau))
text(0.5 + .25, cfun(0.5, tau), expression(tau))

tvec <- c(0.1, 0.25, 0.5, 0.75, 0.9)
plot(0, 0, xlim = c(-1, 1), ylim = 0:1, type = "n", main = expression(tau %in% list(0.1, 0.25, 0.5, 0.75, 0.9)),
    xlab = "x", ylab = expression(rho[tau]))
for (i in seq_along(tvec)) {
  curve(cfun(x, tvec[i]), c(-1, 1), lty = i, col = i, add = TRUE)
}
```

## Moving to regression models
- A linear quantile regression model is a model of conditional quantiles like
  this:
  \begin{equation*}
    Q_{Y \rvert X}(\tau | X = x) = x\tr \beta(\tau).
  \end{equation*}
- The mean regression model is similar: $\ex{Y | X = x} = x\tr \beta$.
- To think of a structural model with an additive error term, we have
  \begin{equation*}
    Y_i = X_i\tr \beta + U_i, \qquad Q_{U|X}(\tau | X_i) = 0
  \end{equation*}
  instead of $\ex{U_i | X_i} = 0$.
- However, the model could be even more general: sometimes it is written as a
  *nonseparable model*
  \begin{equation*}
    Y_i = X_i\tr \beta(U_i), \quad U_i \sim \text{Unif}(0, 1).
  \end{equation*}
  This allows for more general heterogeneity than an added error term.

## Example: heteroskedasticity
- With a quantile regression model we can easily investigate more
  (heterogeneous) features of the conditional response.
- For example, it is very easy to investigate heteroskedasticity: the 
  location-scale regression model
  \begin{equation*}
    Y_{i} = X_{i}\tr\beta + (X_{i}\tr\gamma) U_{i}, \qquad iid \; U_i
  \end{equation*}
  has a straightforward extension in quantiles:
  \begin{equation*}
    Q_{y\rvert X}(\tau \rvert X_{i}) = X_{i}\tr\beta(\tau) \textcolor{gray}{\; =
    X_{i}\tr \left( \beta + \gamma Q_U(\tau) \right)}
  \end{equation*}
- This is one way of connecting a parameterised structural model to a quantile
  regression model, but a quantile regression isn't restricted to this.

# Estimating the model

## Sample analog
- The \(\tau\)th sample quantile is defined by
  \begin{equation*}
    \hat{q}(\tau) = \argmin_{m \in \mathbb{R}} \sum_{i=1}^{n}
    \rho_{\tau}(X_{i} - m).
  \end{equation*}
  If not uniquely determined, then make \(\hat{q}(\tau)\) the smallest
  possible.
- Think of it like a weighted sum:
  \begin{equation*}
    \sum_{i=1}^{n} \rho_{\tau}(X_{i} - m) = \sum_{i=1}^{n} \left\{ \tau I(X_{i}
    - m \geq 0) + (1-\tau) I(X_{i} - m < 0) \right\} |X_{i} - m|
  \end{equation*}
- The argmin will be where \(m\) has proportion \(\tau\) \(X\)'s to the left
  and \((1-\tau)\) to the right.

## QR objective function
- We can estimate $\beta$ from the last slide with a sample $\{y_i,
  x_i\}_{i=1}^n$ by minimising the objective function
  \begin{equation*}
    \hat{\beta}(\tau) = \argmin_{b} \sum_{i=1}^{n} \rho_{\tau}(y_{i} - x_{i}\tr
    b).
  \end{equation*}
- This is not something that you can minimize with calculus, but advances in
  computational techniques have made it easy to do (this can be written as a
  *linear program*).
- R often has the most up-to-date quantile regression commands, in a package
  called ``quantreg``.

## Sample objective function pictures
- The sum of check functions of residuals looks like a cut-glass bowl.  The sum
  of squared residuals is a parabola.
- This is illustrated with two samples of size $n = 4$ (the dashed vertical
  lines show where they are).

```{r obj, echo=FALSE, fig.align='center', fig.height=5}
set.seed(1977)
n <- 4
x <- rnorm(n)
check <- function(tau, u) {u*(tau - ifelse(u < 0, 1, 0))}
obj <- function(tau, xi, sam) {
	sapply(xi, FUN=function(a) {sum(check(tau, sam - a))})
}
Tau <- .5
s <- seq(min(x) - .5, max(x) + .5, by=.01)
par(mfrow=c(1,2), mar=c(2.1, 2.1, 2.1, 1.1), mgp=2:0, cex=.5)
plot(s, obj(Tau, s, x), type="l", col="forestgreen", main="RQ obj. fcn.")
segments(x, 0, x, obj(Tau, x, x), lty=2, col="dodgerblue4")
obj2 <- function(xi, sam) {
	sapply(xi, FUN=function(a) {sum((a - sam)^2)})
}
plot(s, obj2(s, x), type="l", col="firebrick4", main="LS obj. fcn.")
segments(x, 0, x, obj2(x, x), lty=2, col="dodgerblue4")
```

## How to interpret the model
- Once you have estimated a model, like
  \begin{equation*}
    \hat{Q}_{Y|X}(\tau | X) = X\tr \hat{\beta},
  \end{equation*}
  what do you do with the answers?
- First of all, $\hat{Q}_{Y|X}(\tau | X)$ is the estimated **conditional
  $\tau$th quantile of $Y$ given $X$**.  That is, this is a linear model that is
  aimed at explaining a given *quantile* of $Y$, conditional on $X$.
- Second, assuming exogeneity of $X$, you can interpret
  \begin{equation*}
    \hat{\beta} = \widehat{\nabla_x Q_{Y|X}(\tau | x)}
  \end{equation*}
  as the effect that a perturbation in $X$ has on the conditional quantile of
  $Y$.
- This allows you to ask questions about how covariates co-vary with low- or
  high parts of the conditional distribution of $Y|X$.

## R code example
- ``quantreg`` makes running a quantile regression just like a mean regression.
  The regression function is ``rq()`` (for "regression quantile").
- The argument ``tau`` tells ``rq()`` what quantile level you want.  In this
  example, I asked ``rq()`` for all three conditional quartiles.
  - In case you see ``tau = -1``, that asks the program for *all* possible
    quantiles that you could identify in the sample.

\scriptsize
```{r rq_ex, message=FALSE}
library(quantreg)
set.seed(5555)
n <- 50
tauvec <- 1:3 / 4
x <- 10 + rt(n, df = 3)
y <- 1 + x + (1 + 0.25 * x) * rt(n, df = 4)
reg <- rq(y ~ x, tau = tauvec)
```
\normalsize

## Regression summary example
- Just like with ``lm()``, ``rq()`` returns coefficient estimates.
- There is also a ``summary()`` function for ``rq()``.  We will see more about
  that later.

\scriptsize
```{r rq_summary}
print(reg$coef)
```
\normalsize

## A picture of the regression lines: code
- I have estimated three conditional quartiles of $Y$ given $X$.
- Here, the different lines plot my estimates of the conditional quantiles.
- I also plot the true conditional quantiles.  I can calculate them because the
  data are like the location-scale model from a few slides ago.

\scriptsize
```{r rq_plot_code, eval=FALSE}
plot(x, y, main = "Estimated conditional quartiles", pch = 20)
for (i in seq_along(tauvec)) {
  abline(reg$coef[, i], lty = 2, col = "navyblue")
  # True quantiles here:
  abline(a = 1 + qt(tauvec[i], df = 2), b = 1 + 0.25 * qt(tauvec[i], df = 2),
  lty = 3, col = "red")
}
legend("topleft", legend = c("Estimate", "Population"), col = c("navyblue",
  "red"), lty = 2:3, bty = "n")
```
\normalsize

## The picture

```{r rq_plot, echo=FALSE}
plot(x, y, main = "Estimated conditional quartiles", pch = 20)
for (i in seq_along(tauvec)) {
  abline(reg$coef[, i], lty = 2, col = "navyblue")
  # True quantile next
  abline(a = 1 + qt(tauvec[i], df = 2), b = 1 + 0.25 * qt(tauvec[i], df = 2),
  lty = 3, col = "red")
}
legend("topleft", legend = c("Estimate", "Population"), col = c("navyblue",
  "red"), lty = 2:3, bty = "n")
```

## Comparing solutions
- Just as the \(\tau\)th quantile of a sample is equal to one of the
  observations, quantile regression \(\hat{\beta}(\tau)\)s interpolate \(p\)
  points.
- On the other hand, \(\hat{\beta}_{OLS}\) uses all sample points.
	- Can make \(\hat{\beta}_{OLS}\) more efficient.
	- Also makes it less robust to data problems.
- This means that if you look at "quantile residuals", don't be surprised to see
  some that are zero.
- **Also**: note that some of the estimated lines cross on the left!  That is
  embarrassing.  But they are straight lines, so they have to cross somewhere.

## Rearrangement
- If you need to, you can rearrange the quantile function.
- From the previous example, this looks like non-monotonicity of the quantile
  function for $x$ less than about 6.5.
- It can be shown that when this is the case, sorting the predictions is
  actually closer to the truth than the lines.
- There is a function called ``rearrange()`` that rearranges nonmonotone step
  functions back into monotone order (useful if you use ``tau = -1`` to look at
  the whole quantile function).
- The big **Lesson** is that you shouldn't extrapolate linear models too far
  away from the centre of the observed data.

## All the quantile levels
- If you have crossing quantile levels, what should you do?
- Imagine asking for all the estimated quantile curves using 
  ``rq(y ~ x, tau = -1)``.  This produces lots of lines, and many may cross for
  extreme $X$.
- Here is the code for two pictures on the next slide.
- In one, I ask for predicted $\hat{Q}_{Y|X}(\tau | X)$ two times: once when $X$
  is equal to the sample median and once where $x = 5$, a pretty low level for
  $X$.

\scriptsize
```{r cross_code, eval=FALSE}
reg <- rq(y ~ x, tau = -1)
par(mfrow = c(1, 2), mar = c(3.1, 3.1, 1.1, 1.1), mgp = 2:0)
pred_middle <- predict(reg, newdata = list(x = median(x)), stepfun = TRUE)
pred_low <- predict(reg, newdata = list(x = 5), stepfun = TRUE)
plot(pred_middle, do.points = FALSE, main = "Estimates at median of x", ylab =
  expression(Q[Y|X](tau | x)))
plot(pred_low, do.points = FALSE, main = "Estimates at low x", ylab =
  expression(Q[Y|X](tau | x)))
plot(rearrange(pred_low), do.points = FALSE, col = "blue", add = TRUE)
```
\normalsize

## Rearrangement in pictures

```{r cross_plot, echo=FALSE, fig.align = 'center', fig.height=5}
reg <- rq(y ~ x, tau = -1)
par(mfrow = c(1, 2), mar = c(3.1, 3.1, 1.1, 1.1), mgp = 2:0)
pred_middle <- predict(reg, newdata = list(x = median(x)), stepfun = TRUE)
pred_low <- predict(reg, newdata = list(x = 5), stepfun = TRUE)
plot(pred_middle, do.points = FALSE, main = "Estimates at median of X")
plot(pred_low, do.points = FALSE, main = "Estimates at low X")
plot(rearrange(pred_low), do.points = FALSE, col = "blue", add = TRUE)
```

# Interpreting results

## Quantile treatment effects
- Suppose we have just two groups, and one group receives a treatment while the
  other is a control.
- One proposed interpretation of treatment effects:
  \begin{quote}
    Suppose the treatment adds the amount \(\Delta(x)\) when the response of the
    untreated subject would be \(x\).  Then the distribution \(G\) of the
    treatment responses is that of the random variable \(X + \Delta(X)\) where
    \(X\) is distributed according to \(F\).
  \end{quote}
- Translated into two quantile functions, we can say that
  \begin{equation*}
    \Delta(\tau) = G^{-1}(\tau) - F^{-1}(\tau).
  \end{equation*}
- $\Delta(\tau)$ is called the **quantile treatment effect** at level $\tau$.
- It's different from a "usual" treatment effect because it compares
  *distributions*, which may not be a treatment effect for any individual (if
  they would be at a different rank in their counterfactual distribution).

## Interpreting estimates: estimated QTE
- Estimate
  \begin{equation*}
    \{\hat{\alpha}(\tau), \hat{\beta}(\tau) \} = \argmin_{a, b}
    \sum_{i=1}^{n} \rho_{\tau}(y_{i} - a - b D_{i})
  \end{equation*}
  where \(D_{i} = 1\) if \(i\) received the treatment.
- Then \(\hat{\alpha}(\tau)\) is an estimate of \(F^{-1}(\tau)\) (the control
  group), and
  \begin{equation*}
    \hat{\beta}(\tau) = \hat{\Delta}(\tau) = \hat{G}^{-1}(\tau) -
    \hat{F}^{-1}(\tau).
  \end{equation*}
- To generalize to continuous covariates, we have
  \begin{equation*}
    \beta = \nabla_x Q_{Y|X}(\tau | X = x),
  \end{equation*}
  which is the differential analog of the two-group comparison.

## Example with a binary variable: Chamberlain (1994)
- Dependent variable is wages, and these \(\hat{\beta}(\tau)\)'s are for the
  effect of being in a union.
\begin{center}
  \includegraphics[height=2.5in]{chamberlain_94}
\end{center}

## Example with continuous variables: Abrevaya (2001) (simplified)
- Response is a newborn child's weight, and covariates are:
	- race
	- marriage status
	- gender
	- mother's education (2 categories)
	- mother's age 
	- whether the mother smoked during pregnancy
	- the number of cigarettes smoked per day
- The intercept estimates the birthweight distribution (in kg) for female
  children of 26.5-year-old white, unmarried mothers who have less than a high
  school education and who do not smoke.  
- Red lines are OLS estimates, grey bands are CIs for the
  \(\{\hat{\beta}_{j}({\tau})\}\).

## Estimated effects on conditional distribution
\begin{center}
  \includegraphics[height=3in]{smoke}
\end{center}

# Inference

## Inference under homoskedasticity
- Start with the simple model
  \begin{equation*}
  y = X\beta + u, \quad u \sim F
  \end{equation*}
  and \(U\) has a density \(f\) such that \(f(F^{-1}(\tau)) > 0\).
- Suppose \(\frac{1}{n} X\tr X \cp \Qxx\) and let
  \begin{equation*}
    \omega := \frac{\tau - \tau^{2}}{f^{2}(F^{-1}(\tau))}.
  \end{equation*}
- Then $\hat{\beta}(\tau)$ is consistent and AN, and
  \begin{equation*}
    \sqrt{n} \left( \hat{\beta}(\tau) - \beta(\tau) \right) \cw \mathcal{N}
    \left( \zero, \omega \Qxx^{-1} \right).
  \end{equation*}

## Inference across quantiles
- Different quantiles are related too: for any \(\{\tau_{1}, \tau_{2}\}\), let
  \begin{equation*}
    \omega_{ij} = \frac{ \min \{ \tau_{i} \wedge \tau_{j} \} -
    \tau_{i}\tau_{j}}{f(F^{-1}(\tau_{i})) f(F^{-1}(\tau_{j}))}.
  \end{equation*}
- Then
  \begin{equation*}
    \sqrt{n} \begin{pmatrix} \hat{\beta}(\tau_1) - \beta(\tau_1) \\
    \hat{\beta}(\tau_2) - \beta(\tau_2) \end{pmatrix} \cw \mathcal{N} \left(
    \zero, \begin{bmatrix} \omega_{11} \Qxx^{-1} & \omega_{12} \Qxx^{-1} \\
    \omega_{12} \Qxx^{-1} & \omega_{22} \Qxx^{-1} \end{bmatrix} \right).
  \end{equation*}

## Inference with heteroskedasticity
- When each observation can follow a different distribution, we have something
  more complicated: a sandwich matrix for the variance.
- Define $\Qxx$ as before, and now let
  \begin{equation*}
    J(\tau) = \plim_{n} \frac{1}{n} \sum_{i=1}^{n} f_{y_{i} \rvert
    X_{i}}(Q_{y_{i} \rvert X_{i}}(\tau)) X_{i}X_{i}\tr.
  \end{equation*}
- Then
  \begin{equation*}
    \sqrt{n} \left( \hat{\beta}(\tau) - \beta(\tau) \right) \cw \mathcal{N}
    \left( \zero, \tau (1 - \tau) J^{-1}(\tau) \Qxx J^{-1}(\tau) \right).
  \end{equation*}
- The expression for more than one \(\tau\) is similar.
- The biggest difference, compared to usual robust standard errors, is that now
  the *density function* of the errors is involved.
- Estimating \(f^{-1}(F^{-1}(\tau))\) can be a nuisance...

## Estimating covariance matrices
- To summarize: the asymptotic variance of $\sqrt{n}(\hat{\beta}(\tau) -
  \beta(\tau))$ is 
  - **iid case** \(\frac{\tau(1-\tau)}{f^2(F^{-1}(\tau))} \Qxx^{-1}\)
  - **nid case** \(\tau (1-\tau) J^{-1}(\tau) \Qxx J^{-1}(\tau)\), where
- In both cases a term involving \(1/f_{y\rvert X}(F_{y \rvert X}^{-1}(\tau))\)
  is involved.
- This inverse density function is called the *sparsity* function.

## Example: Engel data
- Log (base 10) of food expenditure regressed on income

\scriptsize
```{r eng_calc, eval=FALSE}
data(engel)
attach(engel)
mod <- log10(foodexp) ~ log10(income)
taus <- c(.1, .25, .5, .75, .9)
colvec <- c("red", "orange", "yellow", "green", "blue")
par(mar=c(2.1, 2.1, 1.1, 1.1), mgp=2:0, cex=.5)
plot(income, foodexp, log="xy")
for (i in seq_along(taus)) {
  abline(rq(mod, tau=taus[i], data = engel), col=colvec[i])
}
```
\normalsize

## Example: Engel plot
- How would we check whether the first and third quartiles had the same slope?

```{r eng_plot, echo=FALSE, fig.align='center', fig.height=5}
data(engel)
attach(engel)
mod <- log10(foodexp) ~ log10(income)
taus <- c(.1, .25, .5, .75, .9)
colvec <- c("red", "orange", "yellow", "green", "blue")
par(mar=c(2.1, 2.1, 1.1, 1.1), mgp=2:0, cex=.5)
plot(income, foodexp, log="xy")
for (i in seq_along(taus)) {
  abline(rq(mod, tau=taus[i], data = engel), col=colvec[i])
}
```

## Estimating the sparsity with homoskedastic data
- The reciprocal of the density, also called the *sparsity*, has to
  be evaluated to conduct inference.
- If the observations are homoskedastic, you can evaluate \(\hat{Q}_{Y\rvert
  X}(\tau)\) on both sides of the appropriate \(\tau\) to estimate the sparsity.
- In ``quantreg``, this process is done automatically when you ask for a
  ``summary()`` of an ``rq`` object.  The argument that controls how standard
  errors are calculated is called ``se``.  For small samples the method used is
  called ``rank``.  This method may not give you symmetric intervals, beware.
- ``summary()`` switches automatically to a method called ``nid`` for large
  samples.  That's ok, ``rank`` is good but it can take a long time.

## Summary assuming homoskedastic data: R code

\scriptsize
```{r nid_inf}
est <- rq(mod, tau = c(1/4, 3/4), data = engel)
esum <- summary(est) # uses se = "rank" by default 
print(esum)
```
\normalsize

## Covariance estimation with heteroskedasticity
- Powell (1991) suggested estimating 
  \begin{equation*}
    J(\tau) = \plim \frac{1}{n} \sum_{i=1}^{n} f_{y_{i}\rvert
    X_{i}}(Q_{Y_{i} \rvert X_{i}}(\tau)) X_{i}X_{i}\tr
  \end{equation*}
  with a kernel estimate (let \(\hat{u}_{i} = y_{i} -
  X_{i}\tr\hat{\beta}(\tau)\))
  \begin{equation*}
    \hat{J}(\tau) = \frac{1}{n h_{n}} \sum_{i=1}^{n} K\left(
    \frac{\hat{u}_{i}}{h_{n}} \right) X_{i}X_{i}\tr.
  \end{equation*}
- \(K\) is a kernel function and \(h_{n}\) is its associated bandwidth.
- The ``summary.rq()`` default is
  \begin{equation*}
    \hat{J}(\tau) = \frac{1}{2n a_{n}} \sum_{i=1}^{n}
    I(|\hat{u}_{i}| < a_{n}) X_{i}X_{i}\tr.
  \end{equation*}
  where \(a_{n}\) have to converge slowly to zero (\(h_{n} \rightarrow 0\) but
  \(\sqrt{n}h_{n} \rightarrow \infty\) is specifically what is needed).

## Summary assuming heteroskedasticity: R code

\scriptsize
```{r ker_inf}
esum <- summary(est, se = "ker")
print(esum)
```
\normalsize

## Using the bootstrap
- Bootstrap suggestions are similar to those for mean regression models:
	- The residual bootstrap doesn't work well outside of iid cases.
	- Pairs bootstrap often works well.
	- Wild bootstrap also usually works well.
- There are (many) more options, because nothing has been shown to be
  significantly better than anything else.
- The wild bootstrap is funny because it can't use a symmetric distribution
  (except at the median).  The most common distribution is
  \begin{equation*}
    W = \begin{cases} 2(1-\tau) & w.p. \; 1-\tau \\ -2\tau & w.p. \; \tau
    \end{cases}.
  \end{equation*}

## Summary with (wild) bootstrap: R code

\scriptsize
```{r boot_inf}
esum <- summary(est, se = "boot", bsmethod = "wild")
print(esum)
```
\normalsize

## Inference: example
- Test for heteroskedasticity using quantile estimates.
- Model food expenditure as a function of income:
  \begin{equation*}
    food = \beta_{0} + \beta_{1} inc + u.
  \end{equation*}
- We can test the above specification (i.e., homoskedastic additive error
  \(u\)) with
  \begin{equation*}
    Q_{food|inc}(\tau) = \beta_{0}(\tau) + \beta_{1}(\tau) inc.
  \end{equation*}
- You could test
  \begin{equation*}
    H_{0}: \textcolor{orange}{\beta(.25)} = \textcolor{green}{\beta(.75)}
  \end{equation*}

## Inference: example code
- Luckily for us, there is a function that will compute Wald tests for equality
  of slopes!
- It is called ``anova()``.
- In the example below, the wild bootstrap is used to compute the covariance
  matrix, with a kernel estimate for standard errors (bootstrap methods not
  implemented).

\scriptsize
```{r anova_code, eval=FALSE}
twoquan <- rq(mod, tau = c(1, 3) / 4, data = engel)
atest <- anova(twoquan, se = "ker")
print(atest)
```
\normalsize

## Inference example: results
- Here's what we get from ``anova.rq()``:

\scriptsize
```{r anova, echo=FALSE}
twoquan <- rq(mod, tau = c(1, 3) / 4, data = engel)
atest <- anova(twoquan, se = "ker")
print(atest)
```
\normalsize

- The statistic is a Wald statistic, scaled and measured against an $F$ critical
  value to be conservative.

# Instrumental variables (sketch)

## Instrumental variables for quantile regression
- We may investigate the conditional distribution of \(Y\rvert X\), even if some
  part of \(X\) is endogenous, using IV methods.
- Currently there are several proposals for IV estimation.
- If your endogenous variable is binary, then Abadie, Angrist and Imbens (2002)
  is perhaps the easiest to use.
- For more general instruments, the current favourite is Chernozhukov & Hansen
  (2006).  See also Ma & Koenker (2006) for control functions.
- Mean regression models use a moment restriction to motivate estimation:
  \begin{equation*}
    \ex{U \rvert Z} = 0.
  \end{equation*}
- C\&H's quantile regression model, specialized to a linear form, looks like
  this:
  \begin{equation*}
    Y = X\tr \beta(U), \quad U \rvert Z \sim U(0, 1).
  \end{equation*}

## The IV-QR model
\begin{equation*}
  Y = X\tr \beta(U), \quad U \rvert Z \sim U(0, 1).
\end{equation*}
- This gives us a moment restriction that is similar to the mean regression
  case.
  \begin{align*}
    \prob{Y \leq X\tr\beta(\tau) \rvert Z} &= \prob{X\tr\beta(U) \leq
    X\tr\beta(\tau) \rvert Z} \\
    {} &= \prob{U \leq \tau \rvert Z} = \tau.
  \end{align*}
- Rearranging/renaming, 
  \begin{equation*}
    \prob{Y \leq X\tr\beta(\tau) \rvert Z} = \tau \Leftrightarrow  \prob{Y -
    X\tr\beta(\tau) \leq 0 | Z} = \tau.
  \end{equation*}
- That is, 0 is the \(\tau^{\mathrm{th}}\) quantile of the RV \(Y -
  X\beta(\tau)\), conditional on \(Z\).
- Therefore we should be able to run a quantile regression of \(Y -
  X\beta(\tau)\) on \(Z\) and 0 should be the value of the objective
  function.

## IV-QR: implementation
- As before, let $X = (X_1, X_2)$, where $X_1$ are endogenous.
  \begin{equation*}
    Q_{Y\rvert X}(\tau \rvert X) = X_1\tr\alpha(\tau) + X_2\tr\beta(\tau).
  \end{equation*}
- Suppose \(Z_1\) are related linearly to $X_1$ and write $Z = (Z_1, X_2)$.
- Then
	- Define a grid of \(\{\alpha_{j}\}_{j=1}^{J}\).
	- For each \(\alpha_{j}\) solve
  \begin{equation*}
    \min_{\beta, \gamma} \sum_{i=1}^{n} \rho_{\tau}( (y_{i} -
    x_{1i}\tr\alpha_{j}) - x_{2i}\tr\beta - z_{1i}\tr\gamma)
  \end{equation*}
    to get \((\hat{\beta}(\alpha_{j}, \tau), \hat{\gamma}(\alpha_{j}, \tau))\)
    for \(j = 1, ... J\).
	- Let \(W_{n} = \frac{1}{n} Z\tr Z\).  Find \(\hat{\alpha}(\tau)\) via
    \begin{equation*}
      \min_{j} \hat{\gamma}\tr(\alpha_{j}, \tau) W_{n} \hat{\gamma}(\alpha_{j},
      \tau).
    \end{equation*}

## Similarity with 2SLS, plus note on inference
- It turns out that 2SLS is very similar: if you solve the problem
  \begin{align*}
    (\hat{\beta}(\alpha), \hat{\gamma}(\alpha)) &= \min_{\beta, \gamma}
    \sum_{i=1}^{n} (y_{i} - x_{1i}\tr\alpha - x_{2i}\tr\beta -
    z_{1i}\tr\gamma)^{2} \\
    \hat{\alpha} &= \argmin_j \hat{\gamma}\tr(\alpha_j) W_{n}
    \hat{\gamma}(\alpha_j), \quad \hat{\beta} = \hat{\beta}(\hat{\alpha})
  \end{align*}
  you will find $(\hat{\alpha}, \hat{\beta})$ is the 2SLS estimator.

