\documentclass[11pt]{article}
\usepackage[utf8]{inputenc}
\usepackage[bitstream-charter, cal=cmcal]{mathdesign}
\usepackage[margin=1in]{geometry}
\usepackage{amsmath, amsthm, latexsym}
\usepackage{tcolorbox}

\usepackage{hyperref}
\hypersetup{colorlinks=true, linkcolor=black, urlcolor=blue}

\newcommand{\tr}{^{\top}}
\newcommand{\zero}{\mathbf{0}}
\newcommand{\one}{\mathbf{1}}
\newcommand{\calS}{\mathcal{S}}

\begin{document}

\begin{flushright}\today\end{flushright}
\begin{center}\textbf{Changing $R\beta = r$ restrictions to
exclusion restrictions in regression models}\end{center}

You can claim that instead of testing $H_0: R\beta = r$, all tests can be
transformed to a case where $\beta = (\beta_1\tr, \beta_2\tr)\tr$, and the
equivalent hypothesis is that $H_0: \beta_2 = \zero_{p_2}$.  Here's
why\footnote{This note expands the answer to the question in
an online solution by Davidson \& MacKinnon, available
\href{http://qed.econ.queensu.ca/ETM/solutions/Exercise_4.8.pdf}{here}.  I've
just added a little more explanation and an example.}.

\begin{tcolorbox}
I'll do this with a running example.  Suppose that the model is $y = X\theta +
u$, more specifically
\begin{equation} \label{unrest}
  y = \alpha + \beta v + \gamma w + \delta x + u,
\end{equation}
and we have the restrictions \(\beta = \gamma\) and \(\delta = 1\).  That
makes the restriction $R\theta = r$ look like
\begin{equation*}
  \begin{bmatrix} 0 & 1 & -1 & 0 \\ 0 & 0 & 0 & 1 \end{bmatrix} \theta =
    \begin{bmatrix} 0 \\ 1 \end{bmatrix}.
\end{equation*}
\end{tcolorbox}

Suppose we have the linear model $y = X\beta + u$ and hypothesis $R\beta = r$.
Assume that $R$ is $q \times p$, for $q \leq p$, and $\text{rank}(R) = q$.
Consider breaking \(R\) and \(\beta\) into two parts,
\begin{equation*}
  R\beta = R_1 \beta_1 + R_2 \beta_2 = r,
\end{equation*}
where we make \(R_2\) a nonsingular \(q \times q\) matrix and \(R_1\) a
\(q \times (p-q)\) matrix that contains the remaining \((p-q)\) columns.
There's more than one way to define $R_1$ and $R_2$, so in the example, that
will just be one way of doing things.

The point of this split is that you can solve for some variables in terms of
others, so you have a general plan for how to rearrange the variables into a
restricted model.  You may need to move columns of \(X\) around accordingly to
keep them with elements of \(\beta_2\).  Rewrite $X$ as $[X_1, X_2]$ to go with
$[R_1, R_2]$; that means that $X_1$ is $n \times (p - q)$ and $X_2$ is $n \times
q$.

\begin{tcolorbox}
I already engineered this specific model so that we don't need to move columns
around.  You need to make the second part an \textbf{invertible} $2 \times 2$
matrix.  Now we have the partitioned pieces
\begin{gather*}
  R = [R_1, R_2], \quad R_1 = \begin{bmatrix} 0 & 1 \\ 0 & 0 \end{bmatrix},
  \quad R_2 = \begin{bmatrix} -1 & 0 \\ 0 & 1 \end{bmatrix}, \\ X_1 =
  \begin{bmatrix} 1 & v \end{bmatrix}, \quad X_2 = \begin{bmatrix} w & x
  \end{bmatrix}.
\end{gather*}
\end{tcolorbox}

Because you've made sure that \(R_2\) nonsingular you can rearrange
\begin{equation*}
  R \beta = r \quad \Leftrightarrow \quad R_1 \beta_1 + R_2 \beta_2 = r
  \quad \Leftrightarrow \quad \beta_2 = R_2^{-1}r - R_2^{-1} R_1 \beta_1.
\end{equation*}
Substituting this form of $\beta_2$ into the model you find that
\begin{align*}
  y &= X_1\beta_1 + X_2 \beta_2 + u \\
  {} &= X_1 \beta_1 + X_2 \left( R_2^{-1}r - R_2^{-1} R_1 \beta_1 \right) + u, \\
  \intertext{which tells you how to rearrange the variables:}
  \Leftrightarrow y - X_2 R_2^{-1}r &= (X_1 - X_2 R_2^{-1} R_1) \beta_1 + u \\
  \intertext{or, redefining,}
  \tilde{y} &= \tilde{X}_1 \beta_1 + u. \label{rest}
\end{align*}

\begin{tcolorbox}
Now you just use the definitions of $X_1$, $X_2$, $R_1$, $R_2$ and $r$:
\begin{align*}
  y - X_2 R_2^{-1} r &= y - \begin{bmatrix} w & x \end{bmatrix} \begin{bmatrix}
  -1 & 0 \\ 0 & 1 \end{bmatrix} \begin{bmatrix} 0 \\ 1 \end{bmatrix} \\
  {} &= y - x,
\end{align*}
while
\begin{align*}
  X_1 - X_2 R_2^{-1} R_1 &= \begin{bmatrix} \one & v \end{bmatrix} -
  \begin{bmatrix} w & x \end{bmatrix} \begin{bmatrix} -1 & 0 \\ 0 & 1
\end{bmatrix} \begin{bmatrix} 0 & 1 \\ 0 & 0 \end{bmatrix} \\
  {} &= \begin{bmatrix} \one & v \end{bmatrix} - \begin{bmatrix} \zero & -w
    \end{bmatrix} \\
  {} &= \begin{bmatrix} \one & v + w \end{bmatrix}.
\end{align*}

Running a regression of $(y - x)$ on $\one$ and $(v + w)$ will minimize the sum
of squared residuals subject to the constraints that $\beta = 1$ and $\gamma =
\delta$.  Note that you probably could have figured this out by thinking about the
restriction, but this general recipe is here to tell you what to do when it's not
obvious.
\end{tcolorbox}

The regression of $y - X_2 R_2^{-1} r$ on the $p - q$ new variables $X_1 - X_2
R_2^{-1} R_1$ is one that you can run just by redefining some variables that
will be equivalent to the least-squares solution under the null restriction.

Finally, we can test using a zero restriction in the augmented (from a
restricted-model standpoint) model
\begin{equation} \label{aug}
  \tilde{y} = \tilde{X}_1 \phi_1 + X_2 \phi_2 + u
\end{equation}

Model~\eqref{aug} without imposing $\tilde{H}_0$ is equivalent to the original
model because for any coefficients $\theta_1$ and $\theta_2$,
\begin{align*}
  X_1 \theta_1 + X_2 \theta_2 &= X_1 \theta_1 + X_2 \theta_2 \pm X_2 R_2^{-1}
  R_1 \\
  {} &= \tilde{X} \theta_1 + X_2 (\theta_2 + R_2^{-1} R_1 \theta_1)
\end{align*}
so the space spanned by the
columns of $X$, \(\calS(X)\), is identical to \(\calS(\tilde{X}, X_2)\).  Therefore
the model~\eqref{aug} is equivalent to the unrestricted model $y = X\beta + u$
and when you estimate it, you will find \(\hat{\phi}_1 = \hat{\beta}_1\).  You
can recover the unrestricted $\hat{\beta}_2$ through the relation
\begin{equation*}
  \hat{\beta}_2 = \hat{\phi}_2 - R_2^{-1} R_1 \hat{\phi}_1 + R_2^{-1} r.
\end{equation*}
The last term comes from the fact that $\beta_2$ is the coefficient of a
regression involving $y$ and not $\tilde{y}$.

In regression~\eqref{aug}, \(\tilde{H}_0: \phi_2 = \zero\) corresponds to the
restriction \(R\beta = r\).  If you only regress $\tilde{y}$ on $\tilde{X}$,
thereby imposing the new hypothesis on the rearranged model~\eqref{aug}, the
coefficient estimates $\tilde{\phi}_1$ you get correspond to the estimates
$\tilde{\beta}_1$ under the restriction.  The restricted $\tilde{\beta}_2$ can
be found through the relation
\begin{equation} \label{rest_arrange}
  \tilde{\beta}_2 = \zero - R_2^{-1} R_1 \tilde{\phi}_1 + R_2^{-1} r.
\end{equation}

\begin{tcolorbox}
In the example, imagine that you only run the rearranged model~\eqref{aug}, with
or without the restriction.

With the restriction, you would find an estimate $\tilde{\phi}_1$.  To translate
that back to the way the model was originally written, i.e., in terms of $y$ and
$X$, follow the restricted coefficient formula~\eqref{rest_arrange}:
\begin{equation*}
  \tilde{\beta} = \begin{bmatrix} \tilde{\phi}_1 \\ - \begin{bmatrix} -1 & 0 \\
  0 & 1 \end{bmatrix} \begin{bmatrix} 0 & 1 \\ 0 & 0 \end{bmatrix}
  \tilde{\phi}_1 + \begin{bmatrix} -1 & 0 \\ 0 & 1 \end{bmatrix}
  \begin{bmatrix} 0 \\ 1 \end{bmatrix} \end{bmatrix} = \begin{bmatrix}
  \tilde{\phi}_{11} \\ \tilde{\phi}_{12} \\ \tilde{\phi}_{12} \\ 1 \end{bmatrix}.
\end{equation*}
The effect of un-rearranging $\tilde{\phi}$ to $\tilde{\beta}$ shows that the
restrictions have been imposed correctly.

On the other hand, the unrestricted coefficients $\hat{\beta}$ can be recovered
from the unrestricted $\hat{\phi}$ by
\begin{equation*}
  \hat{\beta} = \begin{bmatrix} \hat{\phi}_1 \\ \hat{\phi}_2 - \begin{bmatrix}
  -1 & 0 \\ 0 & 1 \end{bmatrix} \begin{bmatrix} 0 & 1 \\ 0 & 0 \end{bmatrix}
  \hat{\phi}_1 + \begin{bmatrix} -1 & 0 \\ 0 & 1 \end{bmatrix}
  \begin{bmatrix} 0 \\ 1 \end{bmatrix} \end{bmatrix} = \begin{bmatrix}
    \hat{\phi}_{11} \\ \hat{\phi}_{12} \\ \hat{\phi}_{21} + \hat{\phi}_{12} \\
    \hat{\phi}_{22} + 1
  \end{bmatrix}.
\end{equation*}
\end{tcolorbox}

\end{document}
