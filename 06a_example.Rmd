---
title: Robust standard errors in a simulation experiment
author: Tom Parker
date: \today
output:
  pdf_document:
    highlight: tango
    latex_engine: lualatex
mainfont: charter
urlcolor: blue
fontsize: 11pt
geometry: margin=1in
---

```{r, setup, include=FALSE}
knitr::opts_chunk$set(cache=TRUE)
```

This is a short report (with one DGP) on how well the standard error estimators
work.

# An aside on GLS

This first part just shows you what an feasible GLS routine would do.  It
"happens" to pick exactly the correct heteroskedasticity structure, so its
variance is quite good.  You could try to calculate the covariance matrix
analytically, but it would be easier to just simulate it many times and find the
variance of all the estimates.

```{r, fgls}
# VERY simple FGLS routine - only intended to illustrate the point.
# Generate data
set.seed(8675309)
n <- 200
gamma <- 1
x <- rnorm(n)
z <- rnorm(n)
y <- 1 + x + z + sqrt(exp(gamma * z)) * rnorm(n)

# Correctly-guessed GLS is best.
reg1 <- lm(y ~ x + z)
uhat1 <- log(reg1$resid^2)
resreg <- lm(uhat1 ~ z)
S <- exp(resreg$fit)
scl <- 1 / sqrt(S)

# Since there are only two covariates it's easy to scale manually:
ys <- y * scl
xs <- x * scl
zs <- z * scl
reg2 <- lm(ys ~ xs + zs)
print(summary(reg2))
print(vcov(reg2)) # the vcov() command extracts the covariance matrix
# You usually use the standard errors like in the summary(reg2) printout:
print(sqrt(diag(vcov(reg2))))
```

# Computing robust standard errors

Here is a little code that shows us how the HC0 covariance estimator is put
together.  You can check the manually-built estimator and the one provided in
the ``sandwich`` package match.

```{r, het_data}
# Generate some heteroskedastic data.
set.seed(8675309)
n <- 1000
x <- rnorm(n)
z <- rnorm(n)
gamma <- 1

y <- 1 + x + z + 10 * sqrt(exp(gamma * z)) * rnorm(n)
pairs(~ x + z + y, cex = 0.25) # Take a look.
```

```{r, hc0_ex}
reg <- lm(y ~ x + z)
# Out-of-the-box standard errors:
print(vcov(reg)) # naive estimator

# Build sandwich matrices by hand:
uhat <- resid(reg)
X <- cbind(1, x, z)
bread <- crossprod(X)
meat <- t(X) %*% diag(uhat^2) %*% X
varest <- solve(bread) %*% meat %*% solve(bread)
vnames <- c("(Intercept)", "x", "z")
dimnames(varest) <- list(vnames, vnames)
print(varest)

# Now do it with the sandwich package.
# This one uses HC0 to compare only because HC0 is easy to do.
library(sandwich) # provides command vcovHC()
print(vcovHC(reg, type = "HC0"))
```

Look how different the variance of the third coefficient is between the naive
and robust standard error estimates.  This is explored in the simulation
experiment below.

# Simulations

Here is a simulation experiment showing what the point of the robust standard
errors is.  It uses the same DGP as in the previous code snippet.  For each
repetition in the code below, new data is generated and the coefficients from a
least-squares regression are saved.  Finally the variance matrix of the
coefficients is shown.  Here you can see the problem in the
standard-standard-error estimator.

```{r, simexp}
# A little simulation experiment
reps <- 1000 # If you want to make it more accurate, raise this number
coefs <- matrix(0, nrow = reps, ncol = 3)
for (i in 1:reps) {
  x <- rnorm(n)
  z <- rnorm(n)
  gamma <- 1
  y <- 1 + x + z + 10 * sqrt(exp(gamma * z)) * rnorm(n)
  R <- lm(y ~ x + z)
  coefs[i, ] <- coef(R) # the loop makes a reps x 3 matrix.
}
print(cov(coefs)) # empirical covariance of the estimates
```

Compare this covariance matrix with the covariance matrices printed in the
previous code snippet.  You will see that the standard error is badly estimated
for ``z``'s coefficient when heteroskedasticity is ignored.  The robust standard
error estimate is much closer to the truth.  Here are just the individual
standard errors stuck together:

```{r, compare}
# Collect everything here and report it in one table:
true_coefs <- sqrt(diag(cov(coefs)))
naive <- sqrt(diag(vcov(reg)))
hc0 <- sqrt(diag(vcovHC(reg, type = "HC0")))
hc1 <- sqrt(diag(vcovHC(reg, type = "HC1")))
hc2 <- sqrt(diag(vcovHC(reg, type = "HC2")))
hc3 <- sqrt(diag(vcovHC(reg, type = "HC3")))
se_matrix <- rbind(true_coefs, naive, hc0, hc1, hc2, hc3)
dimnames(se_matrix)[[1]] <- c("True", "Naive", "HC0", "HC1", "HC2", "HC3")
print(se_matrix)
```

In this simple example, all the HC-something estimators do better than the
estimator that assumes homoskedasticity, but it is hard to say what is best
among them.  Generally you should prefer HC2 or HC3.  Look (back at the top) how
much smaller the GLS coefficient standard errors are!  Too bad it is near
impossible to know the way that $U$ varies with the covariates.
