# Slide source files for ECON 621

These .Rmd files are what I use to produce the .pdf slides that are used in
video lectures for class.  Feel free to copy or download them to re-use the code
in your own projects.

The files are a little different than what you would probably use for generating
normal reports because they are set up to produce slides.  They use an R package
called "binb" formats the slides with a theme called Metropolis.  So if I
wanted to change a source file to make it a more usual-looking document, I would
replace

```
output:
  binb::metropolis:
    includes:
      in_header: custom.tex
```

in the YAML header to

```
output:
  pdf_document:
    includes:
      in_header: custom.tex
```

(This would still look weird because the format would be mostly bullet points.)

There are a few non-.Rmd files as well.  I've included all the files that are
needed to compile the source files into the .pdfs that you see.  Here's a quick
explanation of what is not an .Rmd file.

* I use a shell to do my work (to see what that means, windows users check out
  [Cygwin](https://www.cygwin.com), and apple users, I would recommend
  [iTerm2](https://iterm2.com)), so I use a script to compile my pdfs.  It is
  called ``compile.sh`` (often .sh is used to mark files as "shell scripts").

* Over the years, I've collected a few favorite Latex commands to make
  mathematical typesetting easier.  They are collected in the file
  ``custom.tex``.  R Markdown allows you to include Latex files (extension .tex)
  in the YAML header, and there are so many that it would be annoying to
  include them directly in the header.

* I will include the data files used in the slides in this directory as well.

* This is a git repository.  [Git](https://git-scm.com) is a way of keeping
  track of your work &mdash; if you use Dropbox, for example, just imagine that
  it is like that, but where you manually push updates to the cloud, and you
  have a history of all the changes you've made to the files.  There are a
  number of other files, usually related to Latex compilation, that may show up
  if I make mistakes while I'm working on the file.  The file labeled
  ``.gitignore`` tells git to ignore those files when I update this directory.
  There aren't any pdfs because they are "binary" files, so they would always
  change when I re-compile, and waste space on the server, so I only keep the
  pdfs on my local machine and ignore them.  But you can always compile them
  yourself!

* Also a ``README.md`` file is something that is customarily included in a git
  repository that others can view, so that you know what all is in here.  So
  there is also this file that you're reading right now.

This directory will be updated as I make changes to the files and add new ones,
so expect things to change as time goes by.
