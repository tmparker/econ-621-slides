---
title: Playing with projections
author: Tom Parker
date: \today
output: 
  binb::metropolis:
    includes:
      in_header: custom.tex
fontsize: 11pt
---

```{r,setup, include=FALSE}
knitr::opts_chunk$set(cache=TRUE)
```

\newtheorem{thm}{Theorem}

# Reminder of the model in matrix notation
- Remember the model is for the *random variables* $Y \in \R$, $X \in \R^p$ and
  $U \in \R$, we have
  \[Y = X\tr \beta + U\]
- Next, we assume that we see $n$ observations, and label then $y_1, \ldots,
  y_n$, where $y_i \in \R$ for $i = 1, \ldots n$ and $x_1, \ldots, x_n$, where
  $x_i \in \R^p$ for all $i$.
- Then we assume the model applies to each pair $(y_i, x_i)$, giving us $n$
  equations
  \[
    \begin{bmatrix} y_1 \\ \vdots \\ y_n \end{bmatrix} = \begin{bmatrix} x_1\tr
    \\ \vdots \\ x_n\tr \end{bmatrix} \begin{bmatrix} \beta_1 \\ \vdots \\
    \beta_p \end{bmatrix} + \begin{bmatrix} u_1 \\ \vdots \\ u_n \end{bmatrix},
  \]
  or more compactly, $y = X\beta + u$.

## Pieces of the estimate
- Then we saw that either by minimizing the sum of squared residuals or
  estimating the moment equations $\zero = \ex{XU}$, we arrive at
  \[\hat{\beta} = (X\tr X)^{-1} X\tr y.\]
- Once you use the data to estimate $\hat{\beta}$, you can recombine everything
  to produce your best estimate of those $n$ equations:
  \[
    \begin{bmatrix} y_1 \\ \vdots \\ y_n \end{bmatrix} = \begin{bmatrix} x_1\tr
    \\ \vdots \\ x_n\tr \end{bmatrix} \begin{bmatrix} \hat{\beta}_1 \\ \vdots \\
    \hat{\beta}_p \end{bmatrix} + \begin{bmatrix} \hat{u}_1 \\ \vdots \\
    \hat{u}_n \end{bmatrix} \quad \text{or} \quad y = X\hat{\beta} + \hat{u}.
  \]
- Two things are different in this equation compared to the modeled equation:
  $\hat{\beta}$ is estimated, and then we end up with
  \[\hat{u}_i = y_i - x_i\tr\hat{\beta}\]
  for each $i$.

## Errors and residuals
- Note that **you can never observe $u$**.  You have to know the value of
  $\beta$ to know $u$.  You only ever get to see $\hat{u}$, since you know the
  value of $\hat{\beta}$.
- One other commonly used quantity is your vector of predictions:
  \[\hat{y} = X\hat{\beta}.\]
  These are the best predictions of components of $y$ given $X$.  So another way
  to write everything is
  \[y = X\hat{\beta} + \hat{u} = \hat{y} + \hat{u}.\]

# Linear projections

## Span of a collection of vectors
- Several features of the estimates are easily explained using the geometric
  notion of a **projection**, which is a function that maps vectors as close to
  other sets of vectors as possible.
- To that end, think of the design matrix $X$ as
  \[
    X = [X_{\cdot 1}, X_{\cdot 2}, \ldots, X_{\cdot p}],
  \]
  a collection of $p$ vectors in $\R^n$.
- Now recall the definition of a span of vectors $x_1, \ldots x_p \in \R^p$ is
  \[
  \text{span}(x_1, \ldots x_p) = \{z \in \R^n : z = a_1 x_1 + \ldots + a_p
  x_p, a_i \in \R \text{ for each } i\}.
  \]
  That is, it is the space of all vectors $z$ that are some kind of linear
  combination of the vectors $\{x_i\}$.

## The span of $X$
- If these vectors are arranged like in $X$, then all such $z$ are those for
  which a vector $\alpha \in \R^p$ exists such that
  \[ z = X\alpha. \]
- Define the *span* of the columns of $X$, $\calS(X)$, by
  \[
    \calS(X) = \{z \in \R^n : z = X\alpha \text{ for } \alpha \in \R^p \}.
  \]
- That means for whatever value $\hat{\beta}$ you may estimate, 
  \[ X\hat{\beta} \in \calS(X).\]
- If the columns are linearly independent, this spans a $p$-dimensional
  *subspace* of $\R^n$.

## The orthogonal direction
- Furthermore, what's leftover &mdash; the residual vector $\hat{u}$ &mdash; is
  **orthogonal** to the span of $X$.  Check:
  \begin{align*}
    X\tr \hat{u} &= X\tr (y - X\hat{\beta}) \\
    {} &= X\tr (y - X (X\tr X)^{-1} X\tr y) \\
    {} &= X\tr y - X\tr X (X\tr X)^{-1} X\tr y \\
    {} &= X\tr y - X\tr y = \zero.
  \end{align*}
- That is, $\hat{u}$ isn't in the span of the columns of $X$.
- The \emph{orthogonal complement} of \(\calS(X)\) is defined by
  \begin{equation*}
    \calS^\perp(X) = \left\{ w \in \R^n : w\tr x = 0 \text{ for all } x \in \calS(X) \right\}.
  \end{equation*}
- Also, \(\mathrm{dim}(\calS^\perp(X)) = n-p\) and all \(x \in \R^n\) can be written like \(x = y + z\), where \(y \in \calS(X)\) and \(z \in \calS^\perp(X)\).

## Rewriting the model
- The estimated model can be written as
  \[y = X\hat{\beta} + \hat{u} = X(X\tr X)^{-1} X\tr y + \hat{u}\]
- Recall that you can represent a linear function $L$ taking $k$ inputs and
  producing $\ell$ answers with a matrix.  You can find an $\ell
  \times k$ matrix $A$ so that for any $x \in \R^k$, $L(x) = Ax \in \R^\ell$.
- The term $X(X\tr X)^{-1}X\tr y$ could be an $n \times n$ matrix times $y$.
  Reinterpret like this:
  \[X \alert{\hat{\beta}} = X \alert{(X\tr X)^{-1} X\tr y} \quad \text{becomes}
  \quad \alert{X (X\tr X)^{-1} X\tr} y.\]
- Similarly, 
  \begin{align}
    \hat{u} &= y - X(X\tr X)^{-1} X\tr y \\
    {} &= \alert{(I - X(X\tr X)^{-1}X\tr)} y
  \end{align}
  looks like a linear transformation of $y$.

# Orthogonal projection matrices

## Rewriting the model: introduction to the matrices
- The conceptual rewrite from the last slide is a useful analytical trick with
  many uses.  
- Define
  \begin{equation*}
    P_X = X(X\tr X)^{-1} X\tr
  \end{equation*}
  and
  \begin{equation*}
    M_X = I_n - X(X\tr X)^{-1} X\tr.
  \end{equation*}
- Then we can rewrite
  \begin{align*}
    y &= \hat{y} + \hat{u} \\
    {} &= P_X y + M_X y.
  \end{align*}
- $P_X$ and $M_X$ are called **orthogonal projection matrices** (you'll see why
  soon).

## Properties of the projection matrices
- Because of the trick $P_X y = X \alert{(X\tr X)^{-1} X\tr y}$, $P_X y \in
  \calS(X)$.
- Harder to see, but because $M_X y = y - P_X y$, also $M_X y \in
  \calS^\perp(X)$.
- **Theorem:** If $X$ is $n \times p$ with $n \geq p$, then 
  1. $P_X$ is symmetric ($P_X\tr = P_X$)
  2. $P_X$ is idempotent ($P_X P_X = P_X$)
  3. $P_X$ has $p$ eigenvalues equal to 1 and $n - p$ equal to zero
  4. $\text{tr}(P_X) = \text{rank}(P_X) = p$.
- HW: show the first two properties to yourself!  Eigenvalues are the easiest
  way to show the trace/rank result.  Take a look at Appendix A.9-A.11, p.
  863-865.
- $M_X$ satisfies all the same properties.  

## Getting used to orthogonal projection matrices
- Play with the matrices to get used to them.  Here are some fun facts.
- Because $P_X M_X = \zero_{n\times n}$ (check!), you can write
  \[\hat{y}\tr \hat{u} = y\tr P_X M_X y = 0,\]
  that is, the predictions are orthogonal to the residuals.
- In low dimensions?  Choose two vectors in $\R^2$ and
  compute these quantities: the *scalar* projection of $y$ on $x$ is 
  \(\hat{\alpha} = \langle x, y \rangle / \langle x, x \rangle\).  The *vector*
  projection of $y$ on $x$ is $\hat{\alpha}x$.
- Now note that
\begin{equation*}
\hat{\alpha} x = x \hat{\alpha} = x \frac{x\tr y}{x\tr x} = x (x\tr x)^{-1} x\tr y = P_x y.
\end{equation*}

## Estimating the variance of the errors
- The error variance (in a homoskedastic model) is $\sigma^2 = \ex{U^2}$.  If
  you could see the error realizations, the method of moments estimator for
  $\sigma^2$ would be
  \begin{equation*}
    \hat{\sigma}^2_{impossible} = \frac{1}{n} \sum_{i=1}^n u_i^2.
  \end{equation*}
- Since we can't see $u$ ($\Leftrightarrow$ able to see $\beta$), we might think
  about a "feasible" version of the above estimate:
  \begin{equation*}
    \hat{\sigma}^2 = \frac{1}{n} \sum_{i=1}^n \hat{u}_i^2.
  \end{equation*}
- Note that
  \[ \alert{\hat{u}} = M_X y = M_X (X\beta + u) = M_X X \beta + M_X u \alert{=
  M_X u} \]
  because $M_X$ "annihilates" $X$ (check!).

## Estimating the variance of the errors (2)
- What if we use the residuals to estimate $\sigma^2$?
- Rewrite $\hat{\sigma}^2 = \hat{u}\tr \hat{u} / n$, and note that $\sum_i
  \hat{u}_i^2 = \hat{u}\tr \hat{u}$.  Using $\hat{u} = M_X u$,
  \[ \hat{u}\tr \hat{u} = u\tr M_X\tr M_X u = u\tr M_X M_X u = u\tr M_X u, \]
  because of symmetry and idempotency of $M_X$ in turn.
- Soon we will see how this is related to the usual estimator $\hat{u}\tr \hat{u}
  / (n - p)$.  Mechanically, you can see that
  \[u\tr u - u\tr M_X u = u\tr P_X u \geq 0\]
  since $P_X$ is positive semidefinite.
- That is, $\hat{\sigma}^2 \leq \hat{\sigma}^2_{impossible}$ by construction!

## ANOVA
- The **Analysis of Variance** or **ANOVA** equation is also related to the
  orthogonal projections.
- Once again, because $P_X M_X = \zero$ and $y = P_X y + M_X y (= \hat{y} +
  \hat{u})$, we can decompose the total variation in $y$:
  \begin{align*}
    y\tr y &= (P_X y + M_X y)\tr (P_X y + M_X y) \\
    {} &= y\tr P_X y + y\tr M_X y \\
    {} &= \hat{y}\tr \hat{y} + \hat{u}\tr \hat{u}.
  \end{align*}
- In non-matrix notation:
  \begin{equation*}
    \sum_i y_i^2 = \sum_i \hat{y}_i^2 + \sum_i \hat{u}_i^2.
  \end{equation*}
- You've probably seen this with centered second moments, like this:
  \begin{equation*}
    \sum_i (y_i - \bar{y})^2 = \sum_i (\hat{y}_i - \bar{y})^2 + \sum_i
    \hat{u}_i^2.
  \end{equation*}
  This is the classic ANOVA formula.

## ANOVA and $R^2$
- From the ANOVA equation you can derive $R^2$ for a regression. 
- It's just the sample variance of the $\hat{y}$s divided by the sample variance
  of the $y$s.  
- Specifically,
  \[ R^2 = \frac{(\hat{y} - \bar{y} \one)\tr (\hat{y} - \bar{y} \one)} {(y -
  \bar{y} \one)\tr (y - \bar{y} \one)} = 1 - \frac{\hat{u}\tr \hat{u}} {(y -
  \bar{y} \one)\tr (y - \bar{y} \one)}.\]

# The Frisch-Waugh-Lovell theorem

## Partitioned regressors
- The projections have an important application when we can split $X$
  logically into two regressor subsets, like \(X = \begin{bmatrix} X_1 & X_2
  \end{bmatrix}\), where \(X_1 \in \R^{n\times p_1}\), \(X_2 \in \R^{n \times
  p_2}\), and \(p_1 + p_2 = p\).
- Then you can write the model as
  \begin{align*}
    y &= X\beta + u \\
    {} &= \begin{bmatrix} X_1 & X_2 \end{bmatrix} \begin{bmatrix} \beta_1 \\
    \beta_2 \end{bmatrix} + u
  \end{align*}
where \(\beta_1 \in \R^{p_1}\) and \(\beta_2 \in \R^{p_2}\).

## First of two steps of estimation
- Once you partition $X$, imagine sequentially optimizing the SSE:
  \begin{equation*}
    \min_\beta (y - X\beta)\tr (y - X\beta) = \min_{\beta_1} \min_{\beta_2} (y -
    X_1\beta_1 - X_2 \beta_2)\tr (y - X_1\beta_1 - X_2\beta_2).
  \end{equation*}
- Solve the inner problem: 
  \begin{gather*}
    -2X_2\tr (y - X_1\beta_1 - X_2\beta_2) \stackrel{set}{=} \zero_{p_2} \\
    \Rightarrow \hat{\beta}_2(\beta_1) = (X_2\tr X_2)^{-1} X_2\tr (y - X_1
    \beta_1).
  \end{gather*}
- When you plug this in to the SSE expression for $\beta_2$, you'll see that
  both parts of the quadratic are
  \[y - X_1 \beta_1 - X_2 (X_2\tr X_2)^{-1} X_2\tr (y - X_1\beta_1) = M_2 (y -
  X_1 \beta_1),\]
  where
  \[M_2 = I_n - X_2 (X_2\tr X_2)^{-1} X_2\tr.\]

## Second of two steps
- Given the solution for $\hat{\beta}_2(\beta_1)$, we can rewrite
  \begin{align*}
    \min_{\beta_1} \min_{\beta_2} &(y - X_1\beta_1 - X_2 \beta_2)\tr (y -
    X_1\beta_1 - X_2\beta_2) \\
    {} &= \min_{\beta_1} (y - X_1\beta_1)\tr M_2 M_2 (y -
    X_1\beta_1) \\
    {} &= \min_{\beta_1} (y - X_1\beta_1)\tr M_2 (y - X_1\beta_1).
  \end{align*}
- Now solve this for $\beta_1$.  Use $\nabla_x x\tr A x = 2Ax$ to find
  \begin{gather*}
    -2X_1\tr M_2 (y - X_1\beta_1) \stackrel{set}{=} \zero_{p_1} \\
    \Rightarrow \hat{\beta}_1 = (X_1\tr M_2 X_1)^{-1} X_1\tr M_2 y.
  \end{gather*}
- You can repeat the same arguments with $X_1$ and $X_2$ switched (do it!) to
  find that also 
  \[\hat{\beta}_2 = (X_2\tr M_1 X_2)^{-1} X_2\tr M_1 y,\]
  where similarly, $M_1 = I_n - X_1 (X_1\tr X_1)^{-1} X_1\tr$.

## Interpreting what we just did: The FWL theorem
- Recall that $\hat{y} + \hat{u} = P_X y + M_X y$.  Now look at
  \[\hat{\beta}_1 = (X_1\tr M_2 X_1)^{-1} X_1\tr M_2 y.\]
- $M_X$ "makes residuals" out of whatever response variable you apply it to
  (just like $P_X$ makes regression predictions).
- This way of thinking suggests a way to interpret/implement the above formula.
- **Theorem (Frisch-Waugh-Lovell)**
  1. Regress $y$ on $X_2$ and save the residuals $\tilde{y} = M_2 y$.
  2. Regress $X_1$ on $X_2$ and save the residuals $\tilde{X}_1 = M_2 X_1$.
  3. Regress $\tilde{y}$ on $\tilde{X}_1$. 
  4. Then the coefficients from the third step are equal to $\hat{\beta}_1$ from
  the big regression of $y$ on $X = [X_1, X_2]$.

## First FWL application: demeaning data
- This is used all over the place!  It is what we mean when we say that we have
  "partialled out" other irrelevant regressors or "controlled" for them.  You
  could run the big regression and not report those covariates, but you could
  also partial the other parts out and then run the "partial regression": they
  are the same.
- One application, in the text, is about demeaning variables.  Because an
  intercept term is a vector of 1s, call that $\one$ (when necessary, $\one_n$).
  Note
  \begin{align*}
    P_\one &= \one_n (\one_n\tr \one_n)^{-1} \one_n\tr \\
    {} &= \frac{1}{n} \begin{bmatrix} 1 & 1 & \ldots & 1 \\ 1 & \ddots &
    {} & 1 \\ \vdots & {} & {} & \vdots \\ 1 & {} & {} & 1 \end{bmatrix}
  \end{align*}

## Demeaning cont.
  \[P_\one = \one_n (\one_n\tr \one_n)^{-1} \one_n\tr \]

- So then
  \begin{equation*}
    P_\one X = [ \bar{X}_{\cdot 1}, \bar{X}_{\cdot 2}, \ldots, \bar{X}_{\cdot,
    p} ] := \bar{X}.
  \end{equation*}
  That is, each column of $P_\one X$ is just an $n \times 1$ vector of the
  average of that column.
- Then $M_\one X = (I - P_\one) X = X - \bar{X}$.  In other words,
  \begin{equation*}
    (M_\one X)_{ij} = x_{ij} - \bar{x}_{\cdot j}.
  \end{equation*}
- Adding an intercept to the model takes the means away from the columns of $X$.

## FWL applications beyond the intercept
- This can be taken further: if you have several seasons (often 2, 4, or 12 of
  them), you can make those your $X_2$, and you have instantly-deseasonalized
  observations 
- \alert{Aside}: the general way of thinking about an intercept is that the model has an
  intercept if $\one_n \in \calS(X)$.  So four seasons and no intercept produce
  the same $\hat{y}$ and $\hat{u}$ as three seasons and an intercept (they will
  produce different $\hat{\beta}$ because the covariates are different).
- You could also detrend data this way by including some function of time $f(t)$
  in the observations, if they go over time.

## FWL illustration: the data and the model
- Here is a simple example with aggregate earnings data.  The observations are
  - \texttt{loutpthr}: log output per hour
  - \texttt{lhrwage}: log hourly wages
  - \texttt{lwkhours}: log hours worked
  - \texttt{t}: a time trend (just goes 1, 2, 3\ldots).
- There will be an intercept included implicitly in the code below (R adds one
    if you don't say anything in the default regression command \texttt{lm()}).
- Suppose I would just like to ask whether wages influence worker productivity.
  However, I am concerned that the variables may trend over time, and
  that the number of hours worked will artificially raise output and make
  workers look more productive: I use the model
  \begin{equation*}
    \text{output}_t = \alpha + \beta \text{wage}_t + \gamma \text{hours}_t +
    \delta t + u_t.
  \end{equation*}

## R code
\footnotesize
```{r FWLCode, eval=FALSE}
# Read the data in from EARNS.raw (EARNS.DES is a separate file with 
# variable names).
earn <- read.table("../data/EARNS.raw", header = FALSE)
dimnames(earn)[[2]] <- scan("../data/EARNS.DES", what = "character")[2:15]
# Estimate the full model using lm() (lm = linear model)
bigmod <- lm(loutphr ~ lhrwage + lwkhours + t, data = earn)

# Detrend the data a la FWL:
out2 <- lm(loutphr ~ lwkhours + t, data = earn)$resid
wage2 <- lm(lhrwage ~ lwkhours + t, data = earn)$resid

# Estimate the partial model without the irrelevant variables:
smallmod <- lm(out2 ~ wage2 + 0) # + 0 or - 1 in formula = no intercept
# Print out some coefficients to the screen
cat("Big model:", bigmod$coef, "\n", "Small model:", smallmod$coef)
```
\normalsize

## FWL check
- Here you can see the check that the previous code represents: the coefficients
  are the same.
\footnotesize
```{r FWLCheck, echo=FALSE}
earn <- read.table("../data/EARNS.raw", header = FALSE)
dimnames(earn)[[2]] <- scan("../data/EARNS.DES", what = "character")[2:15]
bigmod <- lm(loutphr ~ lhrwage + lwkhours + t, data = earn)
out2 <- lm(loutphr ~ lwkhours + t, data = earn)$resid
wage2 <- lm(lhrwage ~ lwkhours + t, data = earn)$resid
smallmod <- lm(out2 ~ wage2 + 0)
cat("Big model:", bigmod$coef, "\n", "Small model:", smallmod$coef)
```
\normalsize

## FWL picture
- The left plot shows the original scatterplot of \texttt{loutpthr} and
  \texttt{lhrwage}, which is "contaminated" by the trend and hours worked.  The
  right plot shows the relationship once we partial out those variables (and an
  intercept, note that the centre is $(0,0)$).
```{r FWLPlot, echo=FALSE, fig.height = 6}
par(mfrow = c(1, 2), mar = c(3.1, 3.1, 1.1, 1.1), mgp = 2:0)
plot(earn$lhrwage, earn$loutphr, main = "Original data", xlab = "log wage", 
      ylab = "log output")
plot(wage2, out2, main = "Partial residuals", xlab = "residual (log) wage",
      ylab = "residual (log) output")
abline(lm(out2 ~ wage2), lty = 2, col = "blue")
```

## FWL picture code
\footnotesize
```{r FWLPlotCode, eval=FALSE}
par(mfrow = c(1, 2), mar = c(3.1, 3.1, 1.1, 1.1), mgp = 2:0)
plot(earn$lhrwage, earn$loutphr, main = "Original data", xlab = "log wage", 
      ylab = "log output")
plot(wage2, out2, main = "Partial residuals", xlab = "residual (log) wage",
      ylab = "residual (log) output")
abline(lm(out2 ~ wage2), lty = 2, col = "blue")
```
\normalsize

