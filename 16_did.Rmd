---
title: Difference in differences estimation
author: Tom Parker
date: \today
output:
  binb::metropolis:
    includes:
      in_header: custom.tex
fontsize: 11pt
---

```{r,setup, include=FALSE}
knitr::opts_chunk$set(cache=TRUE)
```

# The effect of policy changes

## Example: minimum wage changes
- Difference in difference estimators are often used to try to detect the effect
  of an economic policy on agents.
- For example, Card and Krueger (1994) examined changes in the minimum wage.
- Classical economic theory suggests that a raise in the minimum wage should
  decrease employment.  C&K noticed that in 1992, New Jersey raised the minimum
  wage.
- Given data from before and after a policy change, how should we measure the
  impact of the policy?

## Differences
- C&K wanted to see if the wage increase lowered employment at fast food
  restaurants.  
- They could look at the simple change in employment before and after the law
  takes effect.  This could be called a *difference* estimator.
- Then all the change in employment is due to the law (in this "model").  The
  *counterfactual* outcome comes from before the law change, and implies that
  employment would stay exactly as before without any change in the law.
- But what if the macroeconomic situation is changing?

## A simple difference
- Maybe it's not good enough just to look at NJ alone.  What if there is a
  trend?  In the made up data below, there is a "policy change" at $t = 6$.  How
  would you be able to tell?

\scriptsize
```{r simpletrend, fig.height=5}
set.seed(333)
y <- 1:10 + c(rep(0, 5), rep(1, 5)) + 0.5 * rnorm(10)
plot(ts(y))
abline(v = 6, lty = 2, col = "red")
```
\normalsize

## Choosing a control
- For a better model we look to a similar state for a counterfactual.
- Hopefully it is a state that had a similar experience, except for the policy
  change.
- C&K chose eastern Pennsylvania.  They thought that eastern PA was affected by
  similar economic shocks as NJ.  The states had similar starting wages in
  restaurants.  But there was no minimum wage increase in PA.
- If we can look at the difference in employment in NJ, and *subtract* the
  difference in PA, we would have a **difference in differences** estimate of the
  effect.

## Example: data preparation
- This code downloads the C&K data (uncomment that line to download it) and
  makes the response variable.

\scriptsize
```{r ckdata}
library(haven) # the foreign package only goes up to Stata 12 files
#ck <- read_dta("https://www.ssc.wisc.edu/~bhansen/econometrics/CK1994.dta")
ck <- read_dta("CK1994.dta")
emp <- 0.5 * ck$emppt + ck$empft + ck$nmgrs
ck <- cbind(emp, ck)
```
\normalsize

## Difference in differences
- You can do this like part of a (funny) two-way table:

\scriptsize
```{r didtab}
did_tab <- by(emp, list(ck$state, ck$time), mean, na.rm = TRUE)
M <- matrix(c(did_tab[2, ], did_tab[1, ]), 2, 2)
tab <- rbind(M, diff(M))
tab <- cbind(tab, c(NA, NA, tab[3, 1] - tab[3, 2]))
dimnames(tab) <- list(c("before", "after", "diff."), c("NJ", "PA", "DiD"))
print(tab)
```
\normalsize

## A regression framework
If there are no other covariates, then we could translate the basic
"experiment" into a simple model
\begin{equation*}
  \text{emp}_{it} = \beta_0 + \beta_1 NJ_i + \beta_2 Inc_t + \theta D_{it} +
  u_{it}
\end{equation*}
where $NJ_i = 1$ if an observation is a New Jersey observation, $Inc_t = 1$ if
the wage has been increased for that observation, and $D_{it} = NJ_i \times
Inc_t$ indexes treatment.

- Then PA's average employment is $\beta_0$ before the increase and $\beta_0 +
  \beta_2$ after.

- Meanwhile, NJ's average is $\beta_0 + \beta_1$ before, and $\sum_{k=1}^3
  \beta_k + \theta$ after.

## Calculating changes
- The changes for each state:
  \begin{align*}
    \Delta_{PA} &= \beta_0 + \beta_2 - \beta_0 = \beta_2 \\
    \Delta_{NJ} &= \beta_0 + \beta_1 + \beta_2 + \theta - \beta_0 - \beta_1 =
    \beta_2 + \theta \\
  \end{align*}
- What is the difference between the differences in each state?
  \begin{equation*}
    \Delta_{NJ} - \Delta_{PA} = \beta_2 + \left( \theta - \beta_2 \right) =
    \beta_3.
  \end{equation*}
- $\hat{\beta}_3$ is the difference in differences (or diff in diff or DiD)
  estimate using the regression model.

## Example using a regression estimator
- Use regression as a calculator!
- A very important detail: **use cluster-robust standard errors**!  In this
  case, you should cluster by ``store``.

\scriptsize
```{r did_reg}
library(sandwich)
did_lm <- lm(emp ~ state * time, data = ck)
vc <- vcovCL(did_lm, cluster = ck$store)
did_summary <- rbind(did_lm$coef, sqrt(diag(vc)))
print(did_summary)
```
\normalsize

## Another way to calculate the DiD estimand
- We could also write the diff in diff model as a two-way fixed effects model:
  \begin{align*}
    \text{emp}_{it} &= \beta_0 + \beta_1 NJ_i + \beta_2 Inc_t + \theta D_{it} +
    u_{it} \\
    {} &= \theta D_{it} + \alpha_i + \nu_t + u_{it}.
  \end{align*}
- This is usually easy to do and focuses attention on the coefficient that
  matters, $\theta$.
- Once again, you should use clustered standard errors, although this may be
  different than the two-way clustering used to estimate $\theta$.

## Example using panel data
- Now we use a FE regression as a calculator.

\scriptsize
```{r did_FE}
library(plm)
pck <- pdata.frame(cbind(emp, ck), index = c("store", "time"))
did_plm <- plm(emp ~ state:I(time == 0), data = pck, effect = "twoways")
#only cluster on store
plm_summary <- summary(did_plm, vcov = vcovHC(did_plm, cluster = "group")) 
print(plm_summary$coef)
```
\normalsize

- The ``I(time == 0)`` is a hack to get the sign right: ``state:time`` interacts
  state with an indicator that ``time == 0`` and give you the opposite sign.

## Two-way FEs and more covariates
- The way with two-way FEs is convenient because you can add variables.
- For example, if we want to add the number of hours a store stays open, to
  control for the fact that more hours will require more employees, we can
  estimate this:
\begin{equation*}
    \text{emp}_{it} = \theta D_{it} + \beta Hrs_{it} + \alpha_i + \nu_t
    + u_{it}.
\end{equation*}

\scriptsize
```{r did_hours}
hours <- plm(emp ~ state:I(time == 0) + hoursopen, data = pck, effect = "twoways")
sum_hrs <- summary(hours, vcov = vcovHC(hours, cluster = "group")) 
print(sum_hrs$coef)
```
\normalsize

## Many treatments
- One other extension is to units that all receive treatment at different times.
- For example, in the US there are 50 states, and each state can often decide
  when to enact a law.
- This can easily be accommodated in the model that we already have.  Until now
  we had $D_{it} = \text{State}_i \times \text{Year}_t$, but in case many
  different states are involved, we just add $D_{it}$ as a 0/1 variable for
  before/after treatment takes effect.
- That means the model looks the same: generally
  \begin{equation*}
    Y_{it} = \theta D_{it} + X_{it}\tr \beta + \alpha_i + \nu_t + u_{it},
  \end{equation*}
  but now $D_{it}$ is a more general treatment indicator.
- This uses other units that did not experience treatment-status change at the
  same time as a control group.

# Checking the model

## Assumptions about the model
- Writing the model as a two-way FE model is helpful for seeing what we need to
  assume about the data.  Generally we have
  \begin{equation*}
    Y_{it} = \theta D_{it} + X_{it}\tr \beta + \alpha_i + \nu_t +
    \varepsilon_{it}.
  \end{equation*}
- Then $\theta$ can be estimated and interpreted as causal if we assume:
  1. That the model is correctly specified
  2. That (using two-way panel terminology) $(\ddot{D}_{it}, \ddot{X}_{it})$ has
     positive definite covariance
  3. That $\ex{X_{it} \varepsilon_{it}} = \zero$
  4. and conditional on $X$, $D_{it}$ and $\varepsilon_{is}$ are *independent*
     for all $s, t$.

## What do the assumptions mean?
- The first assumption is not too unusual.  However, be aware that it imposes an
  individual effect and a time effect (common to all ind.), which may be
  restrictive.  Also, the effects are only location shifts.
- The second assumption demands that we have variation over time in some units,
  and that each time period has across-ind. variation.
- The exogeneity condition for $X$ is standard, but the independence assumption
  for $D$ is stronger.
- *Independence* of $D$ means it's more than just an interaction term.  It means
  that (in this example) minimum wage increases weren't imposed in response to
  something in $\varepsilon$, and hiring behaviour didn't change before the
  increase because of $D$.

## Independence of the treatment
- The hardest part of a diff in diff strategy is the last argument, that $D$ and
  $\varepsilon$ are independent.
- You need to supply a (logical) argument for independence, like with
  instruments.
- **The less-anticipated the policy change is, the better!**
  - e.g. C&K argue that the NJ minimum wage was raised 2 years before it went
    into effect, and the economy had changed drastically in the intervening
    time.
- If possible, you should also argue that nothing else should have changed the
  treated and untreated units in different ways before the treatment.
- As a more general principle, the hard part of DiD methods is to assert that
  the control is comparable with the treatment (before treatment happens).

## Testing independence?
- It's hard to test this directly, but you can do it indirectly.
- The concept of *Granger causality* is that $X_t$ *Granger-causes* $Y_t$ if
  movements in $X_{t-k}$ (for some $k > 0$) are correlated with $Y_t$, while all
  $X_{t+k}$ do not predict $Y_t$ well.
- If $D_{t-k}$ and $\varepsilon_t$ are related, that would violate independence
  (therefore it's indirect).
- To check for Granger causality, run the regression
  $$y_{it} = \sum_{j=0}^J \theta_{(-j)} d_{it-j} + \sum_{k=1}^K \theta_{(+k)}
  d_{it} + x_{it}\tr \beta_i + \nu_t + \alpha_i + \varepsilon_{it}$$
  and test $H_0: \theta_{(-j)} = 0$ for all $j$ jointly.
- This is the sort of test that you would prefer not to reject.

## Adding trends
- You can add unit-level trends as a robustness check (usually the model assumes
  that other units all follow the same trend).
- You can add unit-level trend terms for a model like
  $$y_{it} = \theta d_{it} + x_{it}\tr \beta + \nu_t + \alpha_i + \delta_i t +
  \varepsilon_{it}$$
  if you have enough time periods.
- For example, without $X$ or $D$ and assuming the panel is balanced: 
  - there are $n = NT$ observations and
  - there are $N + N = 2N$ (for ints. & slopes) $+ T = 2N + T$ (for time FE)
    FEs to estimate.
  - So if $NT \geq 2N + T$, you can do it!
- Easiest to use FWL: regress $y_{i}$ on $\alpha_i + \delta_i t$ first, get
  residuals $\dot{y}_{it}$.  Do the same for the other variables, and run a FE
  regression with $\nu_t$ as the only FEs (since $\alpha_i$ have been accounted
  for in the first step).

## Unit-level trend test example
- For unit-level trends, regress
  \begin{equation*}
    \dot{y}_{it} = \dot{x}_{it}\tr \beta + \nu_t + \dot{\varepsilon}_{it}
    \tag{long}
  \end{equation*}
  and
  \begin{equation*}
    y_{it} = x_{it}\tr \beta + \nu_t + \varepsilon_{it} \tag{short}
  \end{equation*}
  and then use these residuals in an F test.
- The only hard thing about the F is counting coefficients: here,
  \begin{equation*}
    F = \frac{ \left( \varepsilon_{it}\tr \varepsilon_{it} -
    \dot{\varepsilon}_{it}\tr \dot{\varepsilon}_{it} \right) / (2N) }{
    \dot{\varepsilon}_{it}\tr \dot{\varepsilon}_{it} / (N(T - 1) - 2N - p) }.
  \end{equation*}
- Accounting: 
  - long model has $2N$ ($\alpha_i, \delta_i$), $T$ $(\nu_t)$, $p$ $(\beta)$
    coefficients
  - short model has $T$ $(\nu_t)$ and $p$ $(\beta)$ coefficients.
  - Df upstairs: $2N + T + p - (T + p) = 2N$.
  - Df downstairs: $NT - 2N - T - p = N(T - 1) - 2N - p$.

## Bootstrap testing 
- One feature of the above models that is often untenable is that the
  $\varepsilon_{it}$ are uncorrelated (within $i$).
- Bertrand, Duflo & Mullainathan (2004) showed that when $\ex{\varepsilon_{is}
  \varepsilon_{it}} \neq 0$, then tests can reject the null $H_0: \theta = 0$
  very easily.
  - They could found many significant effects for fake treatment indicators when
    they ignored serial correlation.
- Clustered standard errors are meant to fight against this.
- The bootstrap solution is called *block bootstrap*: like pairs bootstrap, but
  draw over $i = \{1, \ldots, N\}$ with replacement, and take all of $(y_i,
  X_i)$ to put in the new sample.
- It is important to run this kind of test when the units are aggregate, like
  provinces or states, which tend to have a lot of positive autocorrelation.

