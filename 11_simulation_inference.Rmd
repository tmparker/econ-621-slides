---
title: "Bootstrap inference"
author: Tom Parker
date: \today
output:
  binb::metropolis:
    includes:
      in_header: custom.tex
fontsize: 11pt
---

```{r,setup, include=FALSE}
knitr::opts_chunk$set(cache=TRUE)
```

# Bootstrap reference distributions for inference

## Reminder: pivotal statistics
- Recall the classical linear model with normal errors and known variance
  \(\sigma^2\):
  \begin{equation*}
    y = X\beta + u, \quad u|X \sim \calN (\zero_n, \sigma^2 I_n).
  \end{equation*}
  For this model, letting \(\hat{Q}_j = (X\tr X)^{-1}_{jj}\), the associated
  \(z\) statistic *exactly* follows a normal distribution, conditional on
  $X$:
  \begin{equation*}
    z = \frac{\hat{\beta}_j - \beta_j}{\sigma \sqrt{\hat{Q}_j}} \sim
    \calN(0, 1).
  \end{equation*}
- This follows the same distribution for all the DGPs in the model &mdash; no
  matter what the values of \(\beta\) and \(\sigma\) are.
- If a statistic follows the same distribution for every DGP in the model call
  the statistic *pivotal*, or a *pivot*, for the model.

## A golden rule: if possible bootstrap a pivotal statistic
- When using the bootstrap, the rules about the $t$ statistic are what you
  should shoot for, if you can.
- The important thing about the $t$ statistic is its *pivotality*: it doesn't
  depend on any parameters you have to estimate.
- The basic idea behind a "reference distribution" approach to bootstrap
  inference is this: use a bootstrap DGP to generate
  \begin{equation*}
    t_b^* = \frac{\theta^* - \hat{\theta}}{se^*(\theta^*)}
  \end{equation*}
  for $b = 1, \ldots, B$.  Then use these like you would use the $t$ reference
  distribution.
- This applies to the Wald/LR/score trio too.  You use the bootstrap reference
  distribution in place of the $\chi^2$ reference distribution.

## Asymptotic pivots
- If you alter the model you can easily lose exact pivotality.  Consider
  \begin{equation*}
    y = X\beta + u, \quad u \sim iid (\zero, \sigma^2 I_n).
  \end{equation*}
  Then the test statistic for testing a hypothesis is
  \begin{equation*}
    t = \frac{\hat{\beta}_j - \beta_j}{s \sqrt{\hat{Q}_j}} \sim ?
  \end{equation*}
- As \(n\) goes off to infinity, the distributions of the \(t\) statistic tends
  towards a standard normal distribution.  Then we say that the \(t\) statistic
  is *asymptotically pivotal*.  
- Consider this the second-best option to a pivotal statistic.

## Another asymptotic pivot
- When you add a lagged dependent variable, like
  \begin{equation*}
    y_t = X_t\tr \beta + \alpha y_{t-1} + u_t,
  \end{equation*}
  the model no longer satisfies strict exogeneity.  Then the \(t\)
  statistic doesn't even follow a \(t\) distribution.
- However, asymptotic theory tells us that this statistic is
  *asymptotically* distributed like a normal random variable.
- So in this case, the statistic is asymptotically pivotal but in finite samples
  doesn't follow any known distribution.
- This and the last example are good places to use the bootstrap for a reference
  distribution.

## Aside: how strange is the distribution?
- In the last model, the lagged \(y\) observations cause problems.  
- To bootstrap you need to get more involved in the \(y^*\)-generation step: in
  this case you could simulate \(y^*\) one equation at a time.
- Suppose that you assume errors are normal.
- Then, starting with \(y_0\) (which you already set aside), generate
  \(\{u_t^*\}_{t=1}^T\) from a normal distribution and then define
  \begin{align*}
    y_1^* &= X_1\tr \hat{\beta} + \hat{\alpha} y_0 + u_1^* \\
    y_2^* &= X_2\tr \hat{\beta} + \hat{\alpha} y_1^* + u_2^* \\
    \vdots &\phantom{=} \quad \quad \vdots \\
    y_T^* &= X_T\tr \hat{\beta} + \hat{\alpha} y_{T-1}^* + u_T^*.
  \end{align*}
- That is an example of a **parametric bootstrap** (because there is an
  assumption of a parametric distribution for $u^*$).
- Let's see what the distribution of the $t$ statistic for $\alpha$ looks like.

## Example: setup

\scriptsize
```{r dyn_montecarlo}
set.seed(8675309)
T <- 30
x <- rchisq(T, df = 5)
y0 <- rnorm(1)
y <- double(T)
alpha <- 0.75

# Generate data sequentially
u <- rnorm(T)
y[1] <- 1 + x[1] + alpha * y0 + u[1]
for (i in 2:T) {
  y[i] <- 1 + x[i] + alpha * y[i-1] + u[i]
}
ylag <- c(y0, y[-T])
reg <- lm(y ~ x + ylag)
s <- summary(reg)$sigma
co <- coef(reg)
tstat <- coef(summary(reg))[2, 3]
```
\normalsize

## Example: parametric bootstrap

\scriptsize
```{r dyn_boot_para}
B <- 999
tboot <- double(B)
for (i in 1:B) {
  # Generate new data
  ystar <- double(T)
  ustar <- rnorm(T)
  ys0 <- rnorm(1)
  ystar[1] <- co[1] + co[2] * x[1] + co[3] * y0 + s * ustar[1]
  # Generate bootstrap data sequentially
  for (j in 2:T) {
    ystar[j] <- co[1] + co[2] * x[j] + co[3] * ystar[j-1] + s * ustar[j]
  }
  # The rest is the same recipe as the first example.
  yslag <- c(ys0, ystar[-T])
  # Get simulated t statistic
  rstar <- lm(ystar ~ x + yslag)
  costar <- coef(rstar)
  se_star <- coef(summary(rstar))[3, 2]
  tboot[i] <- (costar[3] - co[3]) / se_star
}
```
\normalsize

## Example: visual check of densities
- This plots the simulated vs. asymptotic distribution

\scriptsize
```{r dyn_t_dens_code, eval=FALSE}
plot(density(tboot), xlab = "t statistics", main = "Densities in the dynamic 
     model")
curve(dnorm(x), lty = 2, col = "blue", add = TRUE)
abline(v = qnorm(0.95), lty = 2, col = "blue")
abline(v = quantile(tboot, 0.95))
legend("topleft", legend = c("normal den.", "bootstrap den."), 
       lty = c(2, 1), col = c("blue", "black"), bty = "n")
```
\normalsize

## Example: densities
```{r dyn_t_dens_plot, echo=FALSE}
plot(density(tboot), xlab = "t statistics", main = "Densities in the dynamic 
     model")
curve(dnorm(x), lty = 2, col = "blue", add = TRUE)
abline(v = qnorm(0.95), lty = 2, col = "blue")
abline(v = quantile(tboot, 0.95))
legend("topleft", legend = c("normal den.", "bootstrap den."), 
       lty = c(2, 1), col = c("blue", "black"), bty = "n")
```

## Using a reference distribution: tests and restricted DGPs
- What do you do with a reference distribution?  You use it like an exact or an
  asymptotic distribution!
- In the following example, $y = \alpha + \beta x + u$ and the slope coefficient
  is really 1, and I test $H_0: \beta = 1$.
- In the code on the next slide, a regression is run that imposes the null
  hypothesis on the model.  
- You should use a restricted model when resampling if possible, where the
  restriction comes from your null hypothesis.  That way, the bootstrap DGP acts
  as if the null is true, which is what you assume when you conduct a test
  (assume the null is true, prove otherwise).

## Example: setup and restricted regression

\scriptsize
```{r boot_test_setup}
n <- 20
x <- rchisq(n, df = 4)
y <- 1 + x + 0.5 * runif(n)
reg <- lm(y ~ x)
co <- coef(reg)
# For the hypothesis H_0: \beta = 1
tstat <- (co[2] - 1) / sqrt(vcov(reg)[2, 2])
rest.reg <- lm(I(y - x) ~ 1)
uhat <- rest.reg$resid
uhat <- uhat / sqrt(1 - hatvalues(rest.reg)) # rescale
```
\normalsize

## Example: creating a reference distribution

\scriptsize
```{r boot_test_ref}
B <- 499
tboot <- double(B)
for (i in 1:B) {
  ystar <- co[1] + x + sample(uhat, size = n, replace = TRUE) # impose H0
  breg <- lm(ystar ~ x)
  tboot[i] <- (coef(breg)[2] - 1) / sqrt(vcov(breg)[2, 2])
}
```
\normalsize
- Now there are 499 bootstrap $t$ statistics.  What should you do with them?

## Example: plotting the distributions
- These distributions look so different (next slide) probably because of the
  weird $U$ and $X$ distributions.

\scriptsize
```{r boot_ref_dist_code, eval=FALSE}
plot(density(tboot), lty = 2, col = "blue", main = "bootstrap distribution")
abline(v = tstat, col = "red")
curve(dt(x, df = n - 2), col = "orange", lty = 4, add = TRUE)
arrows(3, 0.3, tstat + 0.1, 0.3, length = 0.1)
text(3.75, 0.3, "test stat.")
```
\normalsize

## Example: picture of the distributions
```{r boot_ref_dist_plot, echo=FALSE}
plot(density(tboot), lty = 2, col = "blue", main = "bootstrap distribution")
abline(v = tstat, col = "red")
curve(dt(x, df = n - 2), col = "orange", lty = 4, add = TRUE)
arrows(3, 0.3, tstat + 0.1, 0.3, length = 0.1)
text(3.75, 0.3, "test stat.")
```
\normalsize
- These distributions look so different probably because of the weird $U$ and
  $X$ distributions.

## Using an asymptotic reference distribution
- All the bootstrap statistics can be used to find a critical value or a
  \(p\)-value.
- Suppose that in the example the alternative hypothesis is that \(\beta > 1\).  
- If the $t$ statistic follows some mystery $F_T$ distribution, the asymptotic
  way is to assert $F_T \approx \Phi$, where \(\Phi\) is the standard normal
  CDF.
- For this alternative, the asymptotic \(p\)-value associated with the observed
  statistic \(t\) is \(p = 1 - \Phi(t) \approx 1 - F_T(t)\).
- In other words, it is the probability that you observe a draw from the normal
  distribution that is larger than your observed test statistic.
- It would be perfect if you could use $F_T$, but you approximate it with
  $\Phi$.

## Using a bootstrap reference distribution
- Given a bootstrap sample, you can easily find a simulated \(p\)-value for your
  test statistic. 
- You have a bootstrap sample \(\{t^*_b\}_{b=1}^B\) that respect the null
  hypothesis (hopefully).  
- This sample is a discrete distribution that estimates \(F_T\).  Call the EDF
  of the \(\{t^*_k\}\) by
  \begin{equation*}
    \ecdf(x) = \frac{1}{B} \sum_{k=1}^B I(t^*_k \leq x).
  \end{equation*}
- Then a simulated \(p\)-value uses $\ecdf$:
  \begin{align*} 
    \hat{p} &= 1 - \ecdf(t) \\
    {} &= 1 - \frac{1}{B} \sum_{k=1}^B I(t^*_k \leq t) \\
    {} &= \frac{1}{B} \sum_{k=1}^B I(t^*_k > t).
  \end{align*}

## Bootstrap p-values
- The consistency of the bootstrap implies that 
  \begin{equation*}
    \hat{p} = 1 - \ecdf(t) \cp 1 - F_T(t) = p
  \end{equation*}
  no matter what the CDF \(F_T\) is.
- You can extend this to other \(p\)-values of interest by counting \(t^*\) that
  are below your observed \(t\) (for alternatives \(H_1: \beta_j < \beta_{0j}\))
  or counting \(t^*\) such that \(|t^*| > |t|\) (for \(H_1: \beta_j \neq
  \beta_{0j}\)).
- The same strategy can be used for statistics based on quadratic forms.  Just
  change the statistics that you compute.

## The number of repetitions
- The number of repetitions often ends in a 9.  Why?
- It is easiest to see when considering \(p\)-values for tests.  Recall that the
  bootstrapped \(p\)-value is basically the same as finding the rank of the
  observed statistic \(T\) among the resampled observations
  \(\{T_k^*\}_{k=1}^B\).
- It's easy to count all the ways that the observed test statistic could rank
  among the others~--- there are \(B+1\) places that the observed \(T\) could
  fall between the \(\{T_k^*\}\).
- Let \(r\) be the number of resampled statistics that are larger than your
  observed statistic, so that for example \(r = 0\) if \(T\) is larger than all
  the \(T^*_k\) (the \(B+1\) values correspond to \(r = 0, r = 1, \ldots r =
  B\)).
- The bootstrap \(p\)-value, using the definition of \(r\), is then just \(r /
  B\), and you reject the null hypothesis when \(r / B < \alpha\).

## Number of repetitions, continued
- Rewriting this as
  \begin{equation*}
    r < \alpha B,
  \end{equation*}
  you can see that of the \(B+1\) possible ways that \(r\) could be, you will
  reject the test only when \(r \in \{0, 1, 2, \ldots \lfloor \alpha B
  \rfloor\}\), where \(\lfloor \alpha B \rfloor\) is the largest integer that is
  smaller than \(\alpha B\).
- That's \(\lfloor \alpha B \rfloor + 1\) ways to reject.  If you would like the
  size of your test to be *exactly* \(\alpha\), then, you should make it so
  that
  \begin{equation*}
    \frac{\lfloor \alpha B \rfloor + 1}{B + 1} = \alpha \quad \Rightarrow \quad
    \lfloor \alpha B \rfloor + 1 = \alpha (B + 1).
  \end{equation*}
- Because \(\lfloor \alpha B \rfloor + 1\) is an integer, you will only get
  exactly the right size if you choose \(B\) so that \(\alpha (B + 1)\) is an
  integer.  So for example when \(\alpha = 0.05 = 1/20\), you could choose \(B\)
  so that
  \begin{equation*}
    \alpha (B + 1) = \frac{B + 1}{20} \in \mathbb{N}, \quad \Rightarrow \text{
    choose } \quad B \in \{19, 39, 59, \ldots \},
  \end{equation*}
  generally speaking, \(m \times 20 - 1\) for \(m \in \mathbb{N}\).

## Number of repetitions (3)
- If you choose a number other than that, you can lose accuracy in terms of
  rejection probabilities under the null.
- Besides this rule, you should let $B$ get as big as you have patience for.
- For most tests, $B = 999$ is a good rule of thumb.  Feel free to set $B$
  pretty low (like $99$) for first-pass checking.
- If your test takes a long time to compute, you probably want to lower $B$, at
  least until you want to produce a publishable result.
- However, for pretty-big $B$ you only lose a tiny amount of power.  That is why
  round numbers are also common (Hansen doesn't care about the $xx9$ repetition
  rule, for example).

## Bootstrap tests can be better than asymptotic tests
- This is just one example where the bootstrap does better than asymptotic
  testing.  However, it is not very rare that the bootstrap does better.
- Bootstrap reference distributions often work better than asymptotic
  distributions for *pivotal* statistics.
- The following example uses the model with a lagged dependent variable,
  \begin{equation*}
    y_t = \alpha + \beta x_t + \gamma y_{t-1} + u_t.
  \end{equation*}
- The specific DGP is important: I set $\gamma = 0.9$.  This means that previous
  $y$ values have a big influence on $y_t$.  If $\gamma = 1$, then this model
  isn't even estimable because of this influence.
- If $\gamma$ were closer to zero, then asymptotic testing would do about as
  well as the bootstrap.
- There is R code in the git repository (too long to put here).

## Picture

\scriptsize
```{r sim}
knitr::include_graphics("./bootstrap_asymptotic_size_comparison.pdf")
```
\normalsize

# Variance estimation using the bootstrap

## The bootstrap is like the jackknife
- Recall that the jackknife could be used estimate the variance matrix for
  $\hat{\beta}$, and it adapts to the situation if the data is heteroskedastic.
- In the previous set of slides, it was established that the bootstrap can do
  something similar.
- Furthermore, both jackknife and bootstrap can be used to estimate the variance
  of functions of parameters.
- One problem that can occur with the bootstrap is from lack of moments in the
  bootstrap data generating process.
- **One general rule**: if you have the time to estimate the variance more than
  one way, it usually pays off to do it, to make sure that you aren't believing
  or publishing nonsense.

## Variance estimation example: now with a bootstrap
- In the last set of slides, the theory was meant to explain that variance
  estimation using the bootstrap works.  Here you can see how easy it is to
  implement.
- The data here are the same as the previous example.  Recall that we were
  working to find a standard error for
  \begin{equation*}
    \hat{\mu} = \exp \left\{ \hat{\alpha} + \hat{\beta} \times 16 +
    \hat{\sigma}^2 / 2 \right\}.
  \end{equation*}

```{r hansen_data_and_jackknife, echo=FALSE}
# This code is repeated from the previous slides, so it isn't presented in the
# slides here.
cps <- read.table("cps09mar.txt")
#cps <- read.table("https://www.ssc.wisc.edu/~bhansen/econometrics/cps09mar.txt")
cps_names <- c("age", "female", "hisp", "educ", "earn", "hours", "week",
  "union", "uncov", "region", "race", "marital")
names(cps) <- cps_names
# Find Hansen's subsample
attach(cps) 
subsam <- cps[(female == 1) & (marital == 1 | marital == 2) & (race == 2) & 
  (age - educ - 6 == 12), ] 
detach(cps)
## Prepare the variables he used
attach(subsam)
dat <- data.frame(lwage = log(earn / (hours * 52)), educ = educ)
detach(subsam)

#####
# This part was from a different snippet in the last slides
n <- nrow(dat)
fun_hansentable <- function(i) {
  dtmp <- dat[-i, ]
  mod_tmp <- lm(lwage ~ educ, data = dtmp)
  betahat <- mod_tmp$coef
  sighat2 <- sum(mod_tmp$resid^2) / (n - 3) 
  muhat <- exp(betahat[1] + 16 * betahat[2] + sighat2 / 2)
  c(betahat, sighat2, muhat)
}
jack <- t(sapply(1:n, fun_hansentable))
dimnames(jack)[[2]] <- c("intercept", "educ", "sigma2", "mu")
all_se_jack <- apply(jack, 2, function(x) sd(x) * (n - 1) / sqrt(n))
```

## Variance estimation: residual bootstrap
- The data (prepared as in the last slides) are called ``dat``.
- This bootstrap variance estimator assumes the errors are homoskedastic.
- The code below saves bootstrap realizations $\beta_b^*$, $\sigma_b^{*2}$ and
  $\mu_b^*$ for $b = 1, \ldots B$.
- Then the estimated standard error is the square root of
  \begin{equation*}
    V^* = \frac{1}{B} \sum_{b=1}^B \left( \theta^*_b - \hat{\theta} \right)^2,
  \end{equation*}
  for any $\theta \in \{\beta, \sigma^2, \mu\}$.
- You can divide by $B$, not $B-1$, because you know the mean of the
  distribution is $\hat{\theta}$.

## The code, part 1: get ready
\scriptsize
```{r hansen_resid1}
B <- 499
mod <- lm(lwage ~ educ, data = dat)
# This part is for bootstrap data generation
wage_hat <- mod$fitted.values
h <- hatvalues(mod)
u_til <- mod$resid / sqrt(1 - h)
# This part is just to calculate mu hat
beta_hat <- mod$coef
sig_hat2 <- sum(mod$resid^2) / (n - 2)
mu_hat <- exp(beta_hat[1] + 16 * beta_hat[2] + sig_hat2 / 2)
```
\normalsize

## The code, part 2: run the bootstrap and calculate the variance
\scriptsize
```{r hansen_resid2}
b_star <- matrix(0, B, 2) # Bx2 matrix of zeros
s_star2 <- double(B) # vector of zeros of length B
mu_star <- double(B) # vector of zeros of length B
for (b in 1:B) {
  wage_star <- wage_hat + sample(u_til, replace = TRUE)
  bootreg <- lm(wage_star ~ educ, data = dat)
  b_star[b, ] <- bootreg$coef # put in bth row
  s_star2[b] <- sum(bootreg$resid^2) / (n - 2) # bth element of vector
  mu_star[b] <- exp(b_star[b, 1] + 16 * b_star[b, 2] + s_star2[b] / 2)
}
# Divide by B, not B - 1, because we know the true means
bstar_centered <- b_star - rep(beta_hat, each = B)
se_betahat_resid <- sqrt(diag(crossprod(bstar_centered))) / sqrt(B)
se_sighat2_resid <- sqrt(sum((s_star2 - sig_hat2)^2) / B)
se_muhat_resid <- sqrt(sum((mu_star - mu_hat)^2) / B)
all_se_resid <- c(se_betahat_resid, se_sighat2_resid, se_muhat_resid)
```
\normalsize

## Reproducing some of Table 10.2 in Hansen
- Now you can compare asymptotic, jackknife and residual bootstrap standard
  errors.
- The residual bootstrap assumes homoskedasticity, so you might want to try
  another bootstrap to check.

\scriptsize
```{r hansen_table_102_pt1}
tab1 <- rbind(c(beta_hat, sig_hat2, mu_hat), all_se_jack, all_se_resid)
dimnames(tab1) <- list(c("estimate", "jackknife", "residual b."), 
                        c("(Intercept)", "educ", "sigma sq.", "mu"))
print(tab1)
```
\normalsize

## Warning!
- As mentioned in the previous notes, you may run into problems with the
  bootstrap if you are a little careless.
- Here is code that produces the illustration from Hansen's text.
- The problem here is that we would like to estimate a maximizer, but it may not
  be that well defined in reality.
- A secondary lesson is that you should look carefully at what you're trying to
  bootstrap, and if you have time, try it a few times (without the same random
  number generator seed) to see if the answer looks stable across simulations.

## Example: function of parameters
- Suppose you have the model
  \begin{equation*}
    \ex{\log \text{wage} | \text{educ}, \text{expr}} = \beta_1 + \beta_2
    \text{educ} + \beta_3 \text{expr} + \beta_4 \text{expr}^2 / 100,
  \end{equation*}
- You would like to estimate the average experience at which a
  worker's (log) wage is highest.
- You also want an estimate of its standard error.
- Calculus and the model tells us that that experience is
  \begin{equation*}
    \text{expr}^* = \frac{-50 \beta_3}{\beta_4}.
  \end{equation*}
- The data used here is the same as before and should come close to matching
  Hansen, p. 280.

## Example: setup
- Get a subsample of married black women to match the regression in Hansen, p.
  280, and set some bootstrap parameters

\scriptsize
```{r warning_example_data}
# Find Hansen's subsample
attach(cps) 
subsam <- cps[(female == 1) & (marital == 1 | marital == 2) & (race == 2), ]
detach(cps)

## Prepare the variables he used
attach(subsam)
dat <- data.frame(lwage = log(earn / (hours * 52)), educ = educ, 
                  expr = age - educ - 6, expr2 = (age - educ - 6)^2 / 100)
detach(subsam)

mod <- lm(lwage ~ educ + expr + expr2, data = dat)
# For the bootstrap
set.seed(321)
n <- nrow(dat)
B <- 499
```
\normalsize

## Example: model estimate

\scriptsize
```{r warning_regression, echo = FALSE}
print(summary(mod))
```
\normalsize

## Example: using the delta method
- If we apply the delta method to the map $\beta \stackrel{g}{\mapsto} -50
  \beta_3 / \beta_4,$ then we need the gradient vector
  \begin{equation*}
    \nabla\tr g(\beta) = \begin{bmatrix} 0 & 0 & -50 / \beta_4 & 50 \beta_3 /
    \beta_4^2 \end{bmatrix}.
  \end{equation*}
- The delta method says that you can estimate an asymptotic standard error for
  \begin{equation*}
    \widehat{\text{expr}}^* = \frac{-50 \hat{\beta}_3}{\hat{\beta}_4}
  \end{equation*}
  with
  \begin{equation*}
    \nabla\tr g(\hat{\beta}) \hat{V} \nabla g(\hat{\beta}).
  \end{equation*}

## Example: the delta method in R
- Here it is implemented in R.  You need to fill in the gradient with an
  estimate using the model coefficient estimates.

\scriptsize
```{r warning_delta}
# Estimate the optimal experience
beta_hat <- mod$coef
theta_hat <- -50 * beta_hat[3] / beta_hat[4]
# Gradient vector
gvec <- c(0, 0, -50 / beta_hat[4], 50 * beta_hat[3] / beta_hat[4]^2)
V <- vcov(mod) # assuming homoskedasticity here.
delta_se <- sqrt(gvec %*% V %*% gvec)
print(delta_se)
```
\normalsize

- I also calculated it with the jackknife and the standard error is close (about
  $7.05$).

```{r warning_jack, echo=FALSE}
# Jackknife standard errors
fun_jack <- function(i) {
  dtmp <- dat[-i, ]
  mod_tmp <- lm(lwage ~ educ + expr + expr2, data = dtmp)
  betahat <- mod_tmp$coef
  -50 * betahat[3] / betahat[4]
}
jack <- sapply(1:n, fun_jack)
jack_se <- sd(jack) * (n - 1) / sqrt(n)
```

## Example: using the bootstrap
- What happens when you use the bootstrap?
- Here is a function that will be repeated ``B`` times to get bootstrap standard
  errors using a residual bootstrap.

\scriptsize
```{r warning_boot_setup}
wage_hat <- mod$fitted.values
h <- hatvalues(mod)
u_til <- mod$resid / sqrt(1 - h)
fun_boot <- function(i) {
  wage_star <- wage_hat + sample(u_til, replace = TRUE)
  bootreg <- lm(wage_star ~ educ + expr + expr2, data = dat)
  bstar <- bootreg$coef 
  -50 * bstar[3] / bstar[4]
}
```
\normalsize

## Example: run the bootstrap a few times to see
- As you can see, I repeated this two more times to compare the standard errors
  for different bootstrap runs.

\scriptsize
```{r warning_boot_runs}
theta1 <- sapply(1:B, fun_boot)
boot_se1 <- sqrt(sum((theta1 - theta_hat)^2) / B)
theta2 <- sapply(1:B, fun_boot)
boot_se2 <- sqrt(sum((theta2 - theta_hat)^2) / B)
theta3 <- sapply(1:B, fun_boot)
boot_se3 <- sqrt(sum((theta3 - theta_hat)^2) / B)
```
\normalsize

## Example: summary
- What do all the standard errors look like?
- Here they are, all together.

\scriptsize
```{r warning_all_ses}
se_vec <- c(delta_se, jack_se, boot_se1, boot_se2, boot_se3)
names(se_vec) <- c("asymptotic", "jackknife", "boot1", "boot2", "boot3")
print(se_vec)
```
\normalsize

- The bootstrap is not stable!  What is it doing?

## Bootstrap distribution: code for a plot
- Here's a density estimate of one.

\scriptsize
```{r dens_plot}
bootsam <- theta1 - theta_hat
plot(density(bootsam))
```
\normalsize

## Example: trimmed bootstrap to the rescue?
- The code below is ad hoc, and begs the question of whether it ought to be run
  at all.  It also should make you go back to the original regression to look
  for a potential cause for this mess.

\scriptsize
```{r trim_code}
# Trimmed bootstrap?
small <- 0.015
theta1_tr <- theta1[theta1 > quantile(theta1, small) & 
                    theta1 < quantile(theta1, 1 - small)] 
theta2_tr <- theta2[theta2 > quantile(theta2, small) & 
                    theta2 < quantile(theta2, 1 - small)]
theta3_tr <- theta3[theta3 > quantile(theta3, small) & 
                    theta3 < quantile(theta3, 1 - small)]
trim_vec <- c(sd(theta1_tr), sd(theta2_tr), sd(theta3_tr))
print(trim_vec) # Still look weird
```

## A critical assessment of the example
- The bootstrap is definitely telling us something.  It is unlikely that the
  bootstrap DGP has a second moment.
- Is that "the bootstrap's fault" or is it your fault?
- It is definitely your fault for reporting it if you don't see that there could
  be a problem!
- Trimming may work to tame the bootstrap DGP, but *why* is the bootstrap DGP so
  wild?  It is supposed to mimic the true DGP.
- The statistical insignificance of $\hat{\beta}_4$ should probably worry you
  about providing standard errors for this estimate.

## Roundup of standard error estimates
- Here are all the estimates at once.

\scriptsize
```{r all_ses}
all_ses <- c(se_vec, trim_vec)
names(all_ses)[6:8] <- c("trim1", "trim2", "trim3")
print(all_ses)
```
\normalsize

- What should we believe?

\scriptsize
- This reminds me of an anecdote about John Tukey: for the holidays he gave the
  statistics team at Bell Labs books of crossword puzzles.  The books had all
  had the answers removed from the back.  Tukey explained that conducting
  statistical analysis is like doing a crossword puzzle without being able to
  see the solutions. 
\normalsize

# Bootstrap-based confidence intervals

## Inverting tests
- Confidence intervals are created by *inverting* a test.  That means that you
  use a test statistic to decide what is in the CI: if you don't reject the
  hypothesis, then that hypothesis is in the CI.
- A CI for $\beta_{j0}$ using a $t$ test and asymptotic theory does it like
  this:
  \begin{align*}
    \textcolor{blue}{-t_{1 - \alpha / 2, n-p}} &\leq \frac{\hat{\beta} -
    \beta}{s} \leq \textcolor{red}{t_{1 - \alpha / 2, n-p}} \\
    \Leftrightarrow -\hat{\beta} \textcolor{blue}{-} s \textcolor{blue}{t_{1 -
    \alpha / 2, n-p}} &\leq - \beta \leq - \hat{\beta} \textcolor{red}{+} s
    \textcolor{red}{t_{1 - \alpha / 2, n-p}} \\
    \Leftrightarrow \hat{\beta} \textcolor{red}{-} s \textcolor{red}{t_{1 -
    \alpha / 2, n-p}} &\leq \beta \leq \hat{\beta} \textcolor{blue}{+} s
    \textcolor{blue}{t_{1 - \alpha / 2, n-p}}.
  \end{align*}
- This results in 
  \begin{equation*}
    CI_t = \left[ \hat{\beta} - s t_{1 - \alpha / 2, n-p}, \hat{\beta} +
    s t_{1 - \alpha / 2, n-p} \right].
  \end{equation*}
- Good confidence intervals are usually found by inverting good tests, where a
  "good" test is one that you can get as close to asymptotically pivotal as
  possible.

## Inverting bootstrap tests: asymmetry (1)
- This is very similar to the testing strategy, except that you don't need to
  restrict the bootstrap DGP.
- However, the bootstrap distribution *does not need to be symmetric*.
- Suppose that your observed statistic \(T\) falls below the median of the
  bootstrap distribution \(\hat{F}_{T^*}\).  Then the bootstrap \(p\)-value of
  this statistic would be
  \begin{equation*}
    p^* = 2 \hat{F}_{T^*}(T) = \frac{2}{B} \sum_{k=1}^B I(T^*_k \leq T).
  \end{equation*}
- You would reject the null hypothesis when \(p^* < \alpha\), which means,
  letting \(r^*\) be the number of \(t^*\) that are less than or equal to \(T\),
  \begin{equation*}
    \text{do not reject when } \frac{2 r^*}{B} \geq \alpha \Leftrightarrow r^*
    \geq \frac{\alpha B}{2}.
  \end{equation*}

## Inverting bootstrap tests: asymmetry (2)
- When \(T\) is a \(t\) statistic, it is easiest to see that for large values of
  \(\beta_0\), \(\hat{\beta} - \beta_0\) becomes increasingly negative and will
  eventually make \(r^* \rightarrow 0\).
- In general \(\alpha B / 2\) is not an integer, so you use the smallest integer
  that is greater than or equal to it, that is, \(\lceil \frac{\alpha B}{2}
  \rceil\).
- So there is some (large) value of \(\beta_0\) for which \(r^*(\beta_0) =
  \lceil \frac{\alpha B}{2} \rceil\).
- This should be used for the \emph{upper} endpoint of the confidence interval.  

## Inverting bootstrap tests: asymmetry (3)
- Sort the bootstrap statistics \(T_k^*\) from smallest to largest, and let
  \(T^*_{\alpha / 2} := T^*_{\lceil \alpha B / 2 \rceil}\).  For example, when
  \begin{equation*}
    \alpha = 0.05 \text{ and } B = 499 \quad \Rightarrow \quad \left\lceil
    \frac{\alpha B}{2} \right\rceil = \lceil 12.475 \rceil = 13,
  \end{equation*}
  so you would look for \(T_{13}^*\) in the sorted list of bootstrap statistics.
- Then the upper endpoint of the bootstrap interval for \(\beta\) will be
  \begin{equation*}
    \beta_u = \hat{\beta} - s T^*_{\alpha / 2}.
  \end{equation*}
- Similarly, you can find the lower end of the bootstrap interval by choosing
  the \(B - \lfloor \frac{\alpha B}{2} \rfloor\)th element and calling it
  \(T^*_{1 - \alpha/2}\); once again, for example
  \begin{equation*}
    \alpha = 0.05 \text{ and } B = 499 \quad \Rightarrow \quad B - \left\lfloor
    \frac{\alpha B}{2} \right\rfloor = 499 - \lfloor 12.475 \rfloor = 487,
  \end{equation*}
  so you would search for \(T^*_{487}\) to get the lower endpoint
  \begin{equation*}
    \beta_l = \hat{\beta} - s T^*_{1 - \alpha / 2}.
  \end{equation*}

## Inverting bootstrap tests: roundup
- The point is that a bootstrap CI is 
  \begin{equation*}
    CI = [\hat{\beta} - s T^*_{1 - \alpha / 2}, \hat{\beta} - s T^*_{\alpha /
    2}],
  \end{equation*}
  where the small $T^*_{\alpha / 2}$ is the $\lceil \alpha B / 2 \rceil$ element
  and $T^*_{1 - \alpha / 2}$ is the $B - \lfloor \alpha B / 2 \rfloor$ element
  of the sorted list of bootstrap $t$ statistics.
- Because this is complicated, you could also just use, e.g., ``quantile(tboot,
  alpha / 2)`` and ``quantile(tboot, 1 - alpha / 2)`` in R.  Just be aware that
  you are throwing away a tiny bit of accuracy.

## CI code example: data
- This is just the data generation

\scriptsize
```{r CI_data}
# A basic t interval.
set.seed(8675309)
n <- 20
x <- rt(n, df = 5)
y <- 1 + x + rnorm(n)
reg <- lm(y ~ x)
```
\normalsize

- Note that the true slope parameter is 1 here.

## Asymptotic CI code example
- Here is how you can construct a 95% CI for the slope parameter

\scriptsize
```{r asymptotic_CI}
betahat <- coef(reg)[2]
se.betahat <- sqrt(vcov(reg)[2, 2])
tquan <- qt(0.975, df = n - 2) # two regressors
lower <- betahat - tquan * se.betahat
upper <- betahat + tquan * se.betahat
ci <- c(lower, upper)
names(ci) <- c("lower", "upper")
print(ci)
# ci contains 1 <=> H_0: beta = 1 would not be rejected in a hypothesis test.
```
\normalsize

## Bootstrap CI code example, part 1: run the bootstrap
- Now for the bootstrap:

\scriptsize
```{r bootstrap_CI_run}
B <- 499
alpha <- 0.05
uhat <- resid(reg)
yhat <- predict(reg)
tstar <- double(B)
for (i in 1:B) {
  ustar <- sample(uhat, size = n, replace = TRUE)
  ystar <- yhat + ustar
  rstar <- lm(ystar ~ x)
  sestar <- sqrt(vcov(rstar)[2, 2])
  tstar[i] <- (coef(rstar)[2] - betahat) / sestar
}
```

## Bootstrap CI code example, part 1: run the bootstrap
- Now invert the $t$ statistics to get a CI:

\scriptsize
```{r bootstrap_CI_calc}
tstar <- sort(tstar)
tstar.lo <- tstar[ceiling(alpha * B / 2)]
tstar.hi <- tstar[B - floor(alpha * B / 2)]
b.lo <- betahat - tstar.hi * se.betahat # subtract the upper quantile.
b.hi <- betahat - tstar.lo * se.betahat # subtract the lower quantile.
bci <- c(b.lo, b.hi)
for.printing <- rbind(ci, bci) # Asymptotic in first row, bootstrap in second
dimnames(for.printing)[[1]] <- c("asymptotic", "bootstrap")
print(for.printing)
```

## Bootstrap CI using a variance estimate
- Here's another way to estimate a confidence interval for a parameter.
- If you have a standard error estimate, say $s^*$, using the bootstrap, then
  you can combine it with asymptotic normality to construct a CI like
  \begin{equation*}
    CI = [ \hat{\theta} - s^* z_{1 - \alpha/2}, \hat{\theta} + s^* z_{1 -
    \alpha/2} ].
  \end{equation*}
- This way is acceptable, although it will do worse than the reference
  distribution way described above when the distribution isn't very symmetric.
- There are still more ways to construct CIs; see Hansen's text if you'd like to
  investigate more.  The next-most-usual one is called a *percentile* interval,
  and it is described there along with some variants.
